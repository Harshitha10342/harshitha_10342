package com.app;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

/**
 * This simple java class is a web resource with specified url path, which
 * provides some functionality
 * 
 * @author IMVIZAG
 *
 */
@Path("/message")
public class Message {
	/**
	 * This method is used to process GET type request from client and returns
	 * response
	 * 
	 * @return String type response
	 */
	@GET
	@Path("/get")
	public String displayMessage() {
		return "hello world";
	}

	/**
	 * This method is used to process POST type request from client and returns
	 * response
	 * 
	 * @return
	 */
	@POST
	@Path("/post")
	public String showMsg() {
		return "hello world";
	}
}
