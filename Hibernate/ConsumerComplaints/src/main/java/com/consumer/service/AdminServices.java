package com.consumercomplaints.services;

import java.util.ArrayList;
import java.util.Date;

import com.consumercomplaints.bean.Complaints;
import com.consumercomplaints.bean.Registration;
/**
 * interface for AdminServices
 * and it contains all the services to be performed by admin
 * @author IMVIZAG
 *
 */
public interface AdminServices {
	/**
	 * These are all abstract methods 
	 * and these implementation is provided in Adminservices implementation class
	 * @param service
	 */
	boolean addService(String service);
	boolean deleteService(int key);
	boolean updateService(int key, String update_service);
	ArrayList<Complaints> totalComplaints();
	ArrayList<Registration> totalCustomers();
	
	//These 3 methods are for set the complaint status by using 3 search method
    Complaints checkByReferenceId(int reference_id);
    ArrayList<Complaints> checkByServiceId(String service_id );
	ArrayList<Complaints> checkByConsumerId(String consumer_id);
	
	boolean setComplaintStatus(int reference_id, String consumer_id, String status, Date newdate, String comment);
	
}
