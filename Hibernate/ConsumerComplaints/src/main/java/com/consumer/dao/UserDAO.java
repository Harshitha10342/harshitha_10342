package com.consumer.dao;


import java.util.ArrayList;
import java.util.Map;

import com.consumer.entity.Complaints;
import com.consumer.entity.Registration;
import com.consumer.entity.UpdatedComplaints;
/**
 * interface for user DAO
 * it contains all abstract method for user
 * @author IMVIZAG
 *
 */
public interface UserDAO {
	
	boolean createUserRegistration(Registration register);
	boolean  login(String id, String password);
	String checkAdminOrUser();
	
	boolean searchById(String id);
	boolean validateAadhar(String aadhar);
	
	Map<Integer, String> getAllServices();
	boolean setCompalaints(Complaints complaints);
	ArrayList<UpdatedComplaints> myUpdatedComplaint(String user_id);
	boolean updateMyPassword(String consumer_id, String password);
}
