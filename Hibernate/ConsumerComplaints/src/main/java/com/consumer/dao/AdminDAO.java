package com.consumer.dao;

import java.util.ArrayList;

import com.consumer.entity.Complaints;
import com.consumer.entity.Registration;
import com.consumer.entity.UpdatedComplaints;
/**
 * interface for Admin services
 * this class contains methods for admin operation
 * @author IMVIZAG
 *
 */
public interface AdminDAO {
	
	boolean addExtraService(String service);
	boolean removeService(int key);
	boolean modifySerive(int key, String update_service);
	ArrayList<Complaints> allComplaints();
    ArrayList<Registration> allCustomers();
	Complaints modifyCompliantStatus(int id_fromAdmin);
	boolean insertUpdatedComplaint(UpdatedComplaints updatedComplaints);
	ArrayList<Complaints> allComplaintsByServiceId(String search);
	ArrayList<Complaints> allComplaintsByConusumerId(String search);

}
