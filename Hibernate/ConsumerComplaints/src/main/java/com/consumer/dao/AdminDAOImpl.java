package com.consumer.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.consumer.entity.Complaints;
import com.consumer.entity.Registration;
import com.consumer.entity.UpdatedComplaints;
import com.consumer.util.HibernateUtil;

/**
 * Dao class for admin
 * 
 * @author IMVIZAG
 *
 */
public class AdminDAOImpl implements AdminDAO {
	/**
	 * method for add extra service which can be updated to the User Services Directly.
	 */
	public boolean addExtraService(String service) {
		
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction txn = session.beginTransaction();
		session.save(service);
		System.out.println("saved");
		txn.commit();
		session.close();
		return true;	
	}

	/**
	 * method for removing the service.
	 * After Removing the Service it will automatically updated to the User Services.
	 */
	public boolean removeService(int key) {
		
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction txn = session.beginTransaction();
		Complaints compliants = session.get(Complaints.class, key);
		session.delete(compliants);
		System.out.println("Deleted");
		txn.commit();
		session.close();
		return true;
		
	}

	/**
	 * method for updating the service based on the key.
	 * Here Existing service can be updated and it will updated to User Services.
	 */
	public boolean modifySerive(int key, String update_service) {
		
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction txn = session.beginTransaction();
		session.update(key);
		System.out.println("Updated");
		txn.commit();
		session.close();
		return false;
		
	}

	/**
	 * method for retriving all the complaints details.
	 */
	public ArrayList<Complaints> allComplaints() {

		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction txn = session.beginTransaction();
		List<Complaints> list = session.createQuery("FROM Product").list();
		Iterator<Complaints> iterator = list.iterator();
		while (iterator.hasNext()) {
			Complaints complaints = (Complaints) iterator.next();
			System.out.println("reference_Id"+complaints.getReference_Id()+"complaint_Name"+complaints.getComplaint_Name()+
					"complaint_id"+complaints.getComplaint_id()+"consumer_Id"+complaints.getConsumer_Id()+"service_Id"+complaints.getService_Id()+
					"Date_of_Complaint"+complaints.getDate_of_Complaint());
		}
		txn.commit();
		session.close();
		return (ArrayList<Complaints>) list;
		
	}

	/**
	 * method for retriving all the Registered customers Details.
	 */
	public ArrayList<Registration> allCustomers() {

		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction txn = session.beginTransaction();
		List<Complaints> allCustomers = session.createQuery("FROM Product").list();
		Iterator<Complaints> iterator = allCustomers.iterator();
		while (iterator.hasNext()) {
			Registration registration = new Registration();
			System.out.println("Consumer ID"+registration.getCus_id()+"Password"+registration.getPwd()+"Aadhar"+registration.getAadhar_num());
		}
		txn.commit();
		session.close();
		return null;
	
	}

	/**
	 * method for searching the complaint status based on the reference ID.
	 */
	public Complaints modifyCompliantStatus(int search) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet result_set = null;
		Complaints complaints = new Complaints();
		try {

			String sql = " select * from Complaints where reference_Id = ?";
			con = DBConnection.getCon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, search);
			result_set = ps.executeQuery();

			while (result_set.next()) {

				complaints.setReference_Id(result_set.getInt(1));
				complaints.setComplaint_id(result_set.getInt(2));
				complaints.setConsumer_Id(result_set.getString(3));
				complaints.setService_Id(result_set.getInt(4));
				complaints.setComplaint_Name(result_set.getString(5));
				complaints.setDate_of_Complaint(result_set.getDate(6));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			// closing the connection
			try {
				con.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return complaints;
	}

	/**
	 * method for inserting the updated complaint status from Admin to the Updated Complaints table.
	 */
	public boolean insertUpdatedComplaint(UpdatedComplaints updatedComplaints) {
		Connection con = null;
		PreparedStatement ps = null;
		String sql;
		boolean result = false;

		try {
			con = DBConnection.getCon();
			sql = "insert into complaint_status values(?,?,?,?,?) ";
			ps = con.prepareStatement(sql);
			ps.setInt(1, updatedComplaints.getReference_id());
			ps.setInt(2, updatedComplaints.getConsumer_id());
			ps.setString(3, updatedComplaints.getStatus());
			ps.setDate(4, new Date(updatedComplaints.getDate_of_resolving().getTime()));
			ps.setString(5, updatedComplaints.getComments());

			int row_count = ps.executeUpdate();
			if (row_count == 1) {
				result = true;

			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}
    /**
     * method for retriving the all complaints by Using the Service ID from Complaints Table.
     */
	public ArrayList<Complaints> allComplaintsByServiceId(String service_id) {

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Complaints complaints = null;
		ArrayList<Complaints> complaint_list = new ArrayList<Complaints>();
		
		try {
			String sql = "SELECT * FROM Complaints WHERE service_id = ?";
			con = DBConnection.getCon();
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, service_id);
			rs = pstmt.executeQuery();
			while (rs.next()) {

				complaints = new Complaints();
				complaints.setReference_Id(rs.getInt(1));
				complaints.setComplaint_id(rs.getInt(2));
				complaints.setConsumer_Id(rs.getString(3));
				complaints.setService_Id(rs.getInt(4));
				complaints.setComplaint_Name(rs.getString(5));
				complaints.setDate_of_Complaint(rs.getDate(6));

				complaint_list.add(complaints);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			 //closing the connection
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return complaint_list;
	}
	
	
	
	 /**
     * method for retriving the all complaints by Using the consumer ID from Complaints Table.
     */
	public ArrayList<Complaints> allComplaintsByConusumerId(String consumer_id) {

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Complaints complaints = null;
		ArrayList<Complaints> complaint_list = new ArrayList<Complaints>();
		
		try {
			String sql = "SELECT * FROM Complaints WHERE consumer_id = ?";
			con = DBConnection.getCon();
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1,consumer_id );
			rs = pstmt.executeQuery();
			while (rs.next()) {

				complaints = new Complaints();
				complaints.setReference_Id(rs.getInt(1));
				complaints.setComplaint_id(rs.getInt(2));
				complaints.setConsumer_Id(rs.getString(3));
				complaints.setService_Id(rs.getInt(4));
				complaints.setComplaint_Name(rs.getString(5));
				complaints.setDate_of_Complaint(rs.getDate(6));

				complaint_list.add(complaints);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			 //closing the connection
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return complaint_list;
	}

	
	
}
