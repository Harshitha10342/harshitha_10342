package com.consumer.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
/**
 * pojo class for updated complaints
 * @author IMVIZAG
 *
 */
@Entity
@Table(name = "complaint_status")
public class UpdatedComplaints {
	
	@Column(name = "reference_id")
	int reference_id;
	
	@Column(name = "status")
	String status;
	
	@Column(name = "date_of_resolving")
	Date date_of_resolving;
	
	@Column(name = "comments")
	String comments;
	
	@Column(name = "consumer_id")
	int consumer_id;
	
	
	public int reference_id() {
		return reference_id;
	}
	public void setReference_id(int reference_id) {
		this.reference_id = reference_id;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getDate_of_resolving() {
		return date_of_resolving;
	}
	public void setDate_of_resolving(Date date_of_resolving) {
		this.date_of_resolving = date_of_resolving;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public int getConsumer_id() {
		return consumer_id;
	}
	public void setConsumer_id(int consumer_id) {
		this.consumer_id = consumer_id;
	}
	
}
