package com.abc.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class EmployeeSource
 */
@WebServlet("/SourceServlet")
public class SourceServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * This the source servlet where the password is checked if it is correct or not
	 * If true the servlet page is dispatched to the TargetServlet else it is
	 * redirected to the error.html page
	 */

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String password1 = request.getParameter("pwd1");
		String password2 = request.getParameter("pwd2");

		List<String> courses = new ArrayList<String>();

		courses.add("Java");
		courses.add("UI");
		courses.add("Embedded");
		courses.add("Testing");
		courses.add(".net");

		// HttpSession session = request.getSession();
		RequestDispatcher rd = request.getRequestDispatcher("TargetServlet");
		if (password1.equals(password2)) {
			request.setAttribute("Language", courses);
			rd.forward(request, response);
		} else {
			response.sendRedirect("error.html");
		}

	}

}
