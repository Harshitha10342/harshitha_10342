package com.abc.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class TargetServlet
 */
@WebServlet("/TargetServlet")
public class TargetServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String username = request.getParameter("username");
		String email = request.getParameter("email");

		PrintWriter out = response.getWriter();
		// HttpSession session = request.getSession(false);

		out.println("<html><body>");
		out.println("<h2>" + "UserName: " + username + "</h2>");
		out.println("<h2>" + "Email: " + email + "</h2>");
		out.println("</body></html>");

		List<String> languages = (List<String>) request.getAttribute("Language");
		out.println("<h2>"+"The Courses Offered are: " + "<h2>");
		Iterator<String> i = languages.iterator();
		while (i.hasNext()) {
			String course = i.next();
			out.println(course +"<br>");
		}

		out.close();
	}
}
