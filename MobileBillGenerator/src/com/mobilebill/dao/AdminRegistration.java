package com.mobilebill.dao;
import com.mobilebill.bean.Admin;
import java.util.List;


/**
 * this is the interface in which all the abstract methods are declared
 * 
 * @author RANJHS
 *
 */

public interface AdminRegistration {

	boolean createAdmin(Admin admin);

	List<Admin> displayAdmin();

}