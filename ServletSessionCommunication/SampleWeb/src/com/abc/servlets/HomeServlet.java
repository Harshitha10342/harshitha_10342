package com.abc.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class DisplayServlet
 */
@WebServlet("/HomeServlet")
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String username = request.getParameter("username");
		String email = request.getParameter("email");

		HttpSession session = request.getSession();
		//String sid = session.getId();
		session.setAttribute("myemail", email);

		PrintWriter out = response.getWriter();
		out.println("<html><body>");
		out.println("Name: " + username);
		out.println("<a href = 'TargetServlet'>Click for email</a>");
		//out.println("The Session Id is" + sid);

		out.println("</body></html>");

	}

}
