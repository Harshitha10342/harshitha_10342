package com.myspace.service;

import java.util.List;

import com.myspace.entities.CulturalEvents;

/**
 * This is the service interface which contains all the abstract methods.
 * 
 * @author IMVIZAG
 *
 */
public interface EventService {

	public List<CulturalEvents> findAll();

	public CulturalEvents findEventById(int eventId);

	public boolean deleteEvent(int eventId);

}
