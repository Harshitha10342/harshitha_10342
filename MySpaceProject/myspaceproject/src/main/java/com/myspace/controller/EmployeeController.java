package com.myspace.controller;

import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.myspace.entities.Employee;
import com.myspace.service.EmployeeService;

@RestController
@RequestMapping("/myspace")
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;

	@PostMapping("/employee/create")
	public ResponseEntity<?> createNewAccount(@RequestBody Employee employee) {
		Employee emp = employeeService.save(employee);

		return new ResponseEntity<>("New account is created with id:" + emp.getId(), HttpStatus.CREATED);
	}

//	@GetMapping("/display")
//	public ResponseEntity<List<Employee>> list() {
//		List<Employee> employee = employeeService.findAll();
//		if (employee.isEmpty()) {
//			return new ResponseEntity<List<Employee>>(HttpStatus.NO_CONTENT);
//		}
//		return new ResponseEntity<List<Employee>>(employee, HttpStatus.OK);
//	}

	@PostMapping("/login")
	public ResponseEntity<?> login(@RequestParam("id") int id, @RequestParam("username") String username,
			@RequestParam("password") String password) {

		Employee result = employeeService.findByUsername(id);
		if (result.getUserName().equals(username) && result.getPassword().equals(password)) {
			return new ResponseEntity<>("Login Successfull", HttpStatus.OK);
		}
		return new ResponseEntity<>("Login Failed", HttpStatus.NOT_FOUND);

	}

}
