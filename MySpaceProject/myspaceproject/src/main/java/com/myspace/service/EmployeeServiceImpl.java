package com.myspace.service;


import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.myspace.dao.EmployeeDao;
import com.myspace.entities.Employee;

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeDao employeeDao;



	@Override
	public Employee save(Employee employee) {
		return employeeDao.save(employee);
	}

//	@Override
//	public List<Employee> findAll() {
//		// TODO Auto-generated method stub
//		List<Employee> accountList = new ArrayList<Employee>();
//		Iterable<Employee> iAccount = employeeDao.findAll();
//		Iterator<Employee> iterator = iAccount.iterator();
//
//		while (iterator.hasNext()) {
//			accountList.add(iterator.next());
//		}
//		return accountList;
//	}

	@Override
	public Employee findByUsername(int id) {

		Optional<Employee> emp = employeeDao.findById(id);
		return emp.get();
	}

}
