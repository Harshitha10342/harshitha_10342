package com.myspace.service;

import java.util.List;

import com.myspace.entities.Employee;

public interface EmployeeService {

	public Employee save(Employee employee);

//	public List<Employee> findAll();

	public Employee findByUsername(int id);
}
