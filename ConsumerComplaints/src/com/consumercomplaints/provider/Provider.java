package com.consumercomplaints.provider;

import java.util.ArrayList;
import java.util.Date;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.consumercomplaints.bean.Complaints;
import com.consumercomplaints.bean.Registration;
import com.consumercomplaints.services.AdminServicesIMPL;
import com.consumercomplaints.services.UserServicesIMPL;

/**
 * This is the resource class which acts as web resource which is specified with
 * a URL
 * 
 * @author IMVIZAG
 *
 */

@Path("/provider")
public class Provider {

	/**
	 * This is resource method which performs user login operation for both admin
	 * and user
	 * 
	 * @param username
	 * @param password
	 * @return
	 */

	@GET
	@Path("/login")
	public String login(@QueryParam("username") String username, @QueryParam("password") String password) {

		boolean isExisted = new UserServicesIMPL().login(username, password);
		String result = isExisted ? "valid user" : "invalid user";

		// String person = new UserServicesIMPL().checkPerson();

		return result;
	}

//	/**
//	 * This is resource method for checking whether the logged person is admin/user
//	 * 
//	 * @return
//	 */
//
//	@GET
//	@Path("/checkPerson")
//	public String checkPerson() {
//		String person = new UserServicesIMPL().checkPerson();
//		return person;
//
//	}

	/**
	 * This is resource method for adding the service by the Admin
	 * 
	 * @param service_name
	 * @return
	 */

	@GET
	@Path("/addService")
	public String addService(@QueryParam("service_name") String service_name) {

		boolean isAdded = new AdminServicesIMPL().addService(service_name);

		return isAdded ? "Service Added" : "Something wrong ";

	}

	/**
	 * This is resource method for deleting the service by the Admin
	 * 
	 * @param service_name
	 * @return
	 */

	@GET
	@Path("/deleteService")
	public String deleteService(@QueryParam("key") int key) {

		boolean isDelete = new AdminServicesIMPL().deleteService(key);

		return isDelete ? "Service Deleted" : "Something wrong ";

	}

	/**
	 * This is resource method for updating the service by the Admin
	 * 
	 * @param update_service
	 * @param key
	 * @return
	 */

	@GET
	@Path("/updateService")
	public String deleteService(@QueryParam("key") int key, @QueryParam("update_service") String update_service) {

		boolean isUpdate = new AdminServicesIMPL().updateService(key, update_service);
		return isUpdate ? "Service Updated" : "Something wrong ";

	}

	/**
	 * This is resource method for displaying the total number of complaints made by
	 * the user
	 * 
	 * @return
	 */

	@GET
	@Produces("application/json")
	@Path("/totalComplaints")
	public ArrayList<Complaints> totalComplaints() {

		ArrayList<Complaints> totalComplaints = new AdminServicesIMPL().totalComplaints();

		return totalComplaints;

	}

	/**
	 * This is resource method for displaying all the registered users
	 * 
	 * @return
	 */
	@GET
	@Produces("application/json")
	@Path("/totalCustomers")
	public ArrayList<Registration> totalCustomers() {

		ArrayList<Registration> totalCustomers = new AdminServicesIMPL().totalCustomers();
		return totalCustomers;

	}
	
	/**
	 * This is resource method to set the status of the complaint by the admin
	 * @param reference_id
	 * @param consumer_id
	 * @param status
	 * @param date
	 * @param comment
	 * @return
	 */
	@GET
	@Path("/setComplaintStatus")
	public boolean setComplaintStatus(@QueryParam("reference_id")int reference_id, @QueryParam("consumer_id")String consumer_id,
			@QueryParam("status") String status,@QueryParam("date") Date date,@QueryParam("comment") String comment) {
		 
		boolean setStatus = new AdminServicesIMPL().setComplaintStatus(reference_id, consumer_id, status, date, comment);
		
		return setStatus;
		
	}
	
	/**
	 *  This is resource method to check the complaint by using ReferenceId
	 * @param reference_id
	 * @return
	 */
	@GET
	@Produces("application/json")
	@Path("/checkByReferenceId")
	public Complaints checkByReferenceId(@QueryParam("reference_id") int reference_id) {
		Complaints complaints = new AdminServicesIMPL().checkByReferenceId(reference_id);
		return complaints;
			
	}
	
	/**
	 *  This is resource method to check the complaint by using ServiceId
	 * @param reference_id
	 * @return
	 */
	@GET
	@Produces("application/json")
	@Path("/checkByServiceId")
	public  ArrayList<Complaints> checkByServiceId(@QueryParam("service_id") String service_id) {
		ArrayList<Complaints> complaints = new AdminServicesIMPL().checkByServiceId(service_id);
		return complaints;
			
	}
	
	
	

}
