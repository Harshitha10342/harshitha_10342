package com.consumercomplaints.controller;

import java.util.Scanner;

import com.consumercomplaints.controller.AdminController;
import com.consumercomplaints.controller.UserController;

/**
 * main class
 * 
 * @author BATCH-D
 *
 */
public class Main {

	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {

		UserController userController = new UserController();
		AdminController adminController = new AdminController();

		while (true) {

			System.out.println("------------------------------------------------------------");
			System.out.println("                 Consumer Complaints                       ");
			System.out.println("                 -------------------                        ");
			System.out.println("                    1: User Login / Admin Login                                 ");
			System.out.println("                    2: New Registration                         ");
			System.out.println("                    3: Exit                                 ");
			System.out.println("");
			System.out.println(" Enter the option :  ");

			// reads the input from the user
			String option = sc.next();

			// Switch case for user to choose the options: Admin_login or User_login or
			// New_User_Registration
			switch (option) {
			// case1: Option for Logins
			case "1":
				int attempts = 0;
				boolean flag = false;
				String id = null;
				String login_password = null;
				
				attempts: while (attempts < 3) {
				System.out.println("Enter the Admin/User Id");
				id = sc.next();
				System.out.println("Enter the password");
				login_password = sc.next();
				
			    flag = userController.doLoginResult(id, login_password);
			    if(flag) {
			    	break attempts;
			    }
			    attempts = attempts + 1;
				if (attempts < 3) {
					System.out.println("You have entered a wrong credentials... Try It again!");
				}
				}
				if (flag) {

					System.out.println("-----------------Login Successfull----------------");
					System.out.println(" ");

					// this method is used to check the person logged in as ADMIN/USER.
					String person = userController.whichPerson();

					if (person.equals("user")) {

						userController.userServices(id);
					} else {

						adminController.adminServices();
					}
				} else {

					System.out.println("Login Failed..! Please try to remeber your Credentials.");
					System.out.println("");
					if(attempts == 3) {
						userController.forgetPassword();
					}
				}
				break;

			// case2: Option for New User Registration
			case "2":
				userController.doNewRegistration();
				break;

			// case 3: for exit from the main page
			case "3":
				System.out.println("Thank you, visit again!");
			    System.exit(0);
				break;

			// default case for wrong option
			default:
				System.out.println("wrong option");
				break;
			}
		}
	}
}
