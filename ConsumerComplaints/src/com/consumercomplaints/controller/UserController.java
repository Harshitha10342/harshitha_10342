package com.consumercomplaints.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Scanner;

import com.consumercomplaints.bean.Complaints;
import com.consumercomplaints.bean.Registration;
import com.consumercomplaints.bean.UpdatedComplaints;
import com.consumercomplaints.services.AdminServicesIMPL;
import com.consumercomplaints.services.UserServicesIMPL;
/**
 * controller class for user to use all his services.
 * @author BATCH-D
 *
 */
public class UserController {

	UserServicesIMPL userServicesImpl = new UserServicesIMPL();
	AdminServicesIMPL adminServicesIMPL = new AdminServicesIMPL();
	AdminController adminController = new AdminController();
	Scanner sc = new Scanner(System.in);
     /**
      * method for performin user operation like updaate the password, check complaint status, see all the services
      * @param user_id
      */
	public void userServices(String user_id) {
        
		boolean flag = true;
		while(flag) {
		System.out.println("                  Your Services            ");
		System.out.println("                  -------------            ");
		System.out.println("                  1.To see Services        ");
		System.out.println("                  2.To check my Complaint Status");
		System.out.println("                  3.Update My Password");
		System.out.println("                  4.Check my Old Complaint");
		System.out.println("                  5.Logout..!");
		
		System.out.println("Enter your choice");
		
		String choice = sc.next();

		switch (choice) {
		case "1":
			System.out.println("                  Services List         ");
			System.out.println("                  -------------         ");
			
			//Used to Print all User Services
			printUserServices();
			
			//This method is used to print the complaint given by the User with a Complaint Id
			PrintUserCompalint(user_id);
			break;
			
		case "2":
			System.out.println("                 Here is Your Updated Complaint Details..");
			System.out.println( "         Reference ID\t Status\t\t Date Of Resolving\tComments");
			System.out.println("          ------------   ------          ---------------       --------");
			
			ArrayList<UpdatedComplaints> updatedComplaints = userServicesImpl.myUpdatedComplaintStatus(user_id);
			
			//This method is used to show the User Updated complaint status.
			printMyComplaint(updatedComplaints);
			System.out.println();
			break;
			
		case "3":
			System.out.println("Enter the New Password to Update");
			String password = sc.next();
			
			//This is used to update the password of Existing user after login.
			userServicesImpl.updatingMyPassword(user_id, password);
			
			System.out.println(".....Password is Updated Successfully.....!");
			System.out.println(" ");
			break;
			
		case "4":
			System.out.println("Here is your Old Complaint");
			ArrayList<Complaints> complaints_list = adminServicesIMPL.checkByConsumerId(user_id);
			printOldComplaints(complaints_list);
			break;
			
		case "5":
			System.out.println("....Thank You....");
			System.out.println("");
			flag = false;
			break;
		
		default:
			System.out.println("Inavlid option selected..!");
		}
	  }
	}
    /**
     * method for print all the user services 
     */
	public void printUserServices() {

		Map<Integer, String> user_map = userServicesImpl.fetchAllServices();
		Iterator<Entry<Integer, String>> it = user_map.entrySet().iterator();

		while (it.hasNext()) {

			@SuppressWarnings("rawtypes")
			Map.Entry pair = (Map.Entry) it.next();

			// printing the all services
			
			System.out.println("                  "+pair.getKey() + ". " + pair.getValue());
			it.remove();
		}
	}
    /**
     * method for login
     * @param id
     * @param login_password
     * @return
     */
	public boolean doLoginResult(String id, String login_password) {

		boolean flag = userServicesImpl.login(id, login_password);
		return flag;
	}
    /**
     * method for knowing wich persion is registered
     * @return
     */
	public String whichPerson() {
		String person = userServicesImpl.checkPerson();
		return person;
	}

	public void PrintUserCompalint(String user_id) {

		Random r = new Random();
		System.out.println("Hey User Select a Service!...");
		int choose_service = sc.nextInt();

		Map<Integer, String> user_map = userServicesImpl.fetchAllServices();

		Iterator<Entry<Integer, String>> it2 = user_map.entrySet().iterator();
		while (it2.hasNext()) {

			@SuppressWarnings("rawtypes")
			Map.Entry pair = (Map.Entry) it2.next();
			if (pair.getKey().equals(choose_service)) {

				int complaint_id = r.nextInt((10000 - 100) + 1) + 111;
				
				System.out.println("                  "+pair.getKey() + ". " + pair.getValue());
				System.out.println("                   you are Selected the Service --> " + pair.getKey());
				System.out.println("                   Your complaint               --> "+pair.getValue());
				System.out.println("                   And Your reference id is     --> " + complaint_id);
				System.out.println("");
						

				int service_Id = choose_service;
                
				String complaint_Name = (String) pair.getValue();
				
				userServicesImpl.setComplaint(service_Id, complaint_Name, complaint_id, user_id);
			}
		}
	}

	public void printMyComplaint(ArrayList<UpdatedComplaints> updatedComplaints) {

		Iterator<UpdatedComplaints> itr = updatedComplaints.iterator();

		while (itr.hasNext()) {
			
			UpdatedComplaints updatedComplaint = itr.next();
			
			System.out.println("          "+updatedComplaint.getReference_id() + "           " + updatedComplaint.getStatus() + "           "
					+ updatedComplaint.getDate_of_resolving() + "          " + updatedComplaint.getComments());
		}
	}
   /**
    * method for New user registration
    */
	public void doNewRegistration() {
        
		System.out.println("Enter the New user ID");
		String id1 = sc.next();
		System.out.println("Enter the Password");
		String password = sc.next();
		System.out.println("Enter the Aadhar Number");
		String aadhar = sc.next();

		if (userServicesImpl.validateId(id1) || userServicesImpl.validateAadhar(aadhar)) {

			System.out.println("Invalid data || fields alreday existed..");
		} else {

			Registration registration = new Registration();
			registration.setCus_id(id1);
			registration.setPwd(password);
			registration.setAadhar_num(aadhar);

			boolean result1 = userServicesImpl.newUserRegistration(registration);

			if (result1) {

				System.out.println("You Registered successfully...");
			}
		}
	}
	
	/**
	 * method for forgot password 
	 */
	public void forgetPassword() {
		
		System.out.println("you have reached maximum attempts...."); 
		System.out.println("----------------------");
		System.out.println("enter the aadhar number");
		System.out.println("");
		String aadhar = sc.next();
		ArrayList<Registration> forgetPassword = new AdminServicesIMPL().totalCustomers();
		
		Iterator<Registration> itr = forgetPassword.iterator();

		while (itr.hasNext()) {
			Registration getDetails = itr.next();
			if(getDetails.getAadhar_num().equals(aadhar)) {
				System.out.println("                Your Login Credentials are    ");
				System.out.println("                -------------------------");
				System.out.println("              Consumer ID          Password");
				System.out.println("              ----------           --------");
				System.out.println("              "+getDetails.getCus_id()+"                  "+getDetails.getPwd());
			}
		}
	}
	
	 /**
     * method for print all the complaints
     * @param complaints_list
     */
	public void printOldComplaints(ArrayList<Complaints> complaints_list) {

		// Complaints complaints =null;
		System.out.println(
				"                                            Total complaints                                         ");
		System.out.println(
				"                                            ----------------                                         ");
		System.out.println(
				"Complaint Id\tService Id\tComplaint Name\t\tDate Of Complaint");
		System.out.println(
				"-----------\t----------\t--------------\t\t-----------------");

		Iterator<Complaints> itr = complaints_list.iterator();

		while (itr.hasNext()) {
			Complaints complaints = itr.next();
			System.out.println(complaints.getComplaint_id()+
					 "\t\t " + complaints.getService_Id() + "\t\t "
					+ complaints.getComplaint_Name() + "\t\t " + complaints.getDate_of_Complaint());
		}
	}
}
