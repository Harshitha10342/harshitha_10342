package com.consumercomplaints.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.consumercomplaints.bean.Complaints;
import com.consumercomplaints.bean.Registration;
import com.consumercomplaints.services.AdminServicesIMPL;
/**
 * class for admin controller
 * this class performs operations like add service, delete service, update the service, check toatl complaint received, and logout
 * @author IMVIZAG
 *
 */
public class AdminController {
    //variable for date pattern
	static final String DATE_PATTERN = "(0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[012])/((19|20)\\d\\d)";
	Scanner sc = new Scanner(System.in);
    /**
     * method for providing all the admin services
     */
	public void adminServices() {

		UserController userController = new UserController();
		AdminServicesIMPL adminServicesImpl = new AdminServicesIMPL();

		System.out.println("Admin Operations");
		System.out.println("----------------");

		System.out.println("1. Add service");
		System.out.println("2. Delete Service");
		System.out.println("3. Update Service");
		System.out.println("4. Check total complaints recevied");
		System.out.println("5. Check total number of customers");
		System.out.println("6. Update the Status of Complaint");
		System.out.println("7. LogOut..!");
		System.out.println("");

		// switch case for admin operations....
		boolean flag = true;
		while (flag) {
			System.out.println("");
			System.out.println("enter your choice to use admin services :");
			String choice = sc.next();

			switch (choice) {
            // case 1 for Add an extra services
			case "1":
				System.out.println("services list");
				System.out.println("-------------");
				userController.printUserServices();
				System.out.println("-------------");

				System.out.println("enter the service to Add...");
				String extra = sc.next();

				adminServicesImpl.addService(extra);

				System.out.println("");
				System.out.println("----Service Added----");
				System.out.println(" ");
				break;
            //case 2 : for delete the existing  service
			case "2":
				System.out.println("services list");
				System.out.println("-------------");
				userController.printUserServices();
				System.out.println("-------------");

				System.out.println("enter the service to delete...");
				int key = sc.nextInt();

				adminServicesImpl.deleteService(key);

				System.out.println("");
				System.out.println("---service deleted----");
				System.out.println("");
				break;
           //case 3: for update the existing service
			case "3":
				System.out.println("services list");
				System.out.println("-------------");
				userController.printUserServices();
				System.out.println("-------------");

				System.out.println("enter the key ");
				int service_id = sc.nextInt();

				System.out.println("enter service to update...");
				String update_service = sc.next();

				adminServicesImpl.updateService(service_id, update_service);
				System.out.println("---service updated----");
				System.out.println("");
				break;
            // case 4: for displaying the total complaints
			case "4":
				ArrayList<Complaints> rs = adminServicesImpl.totalComplaints();
				printComplaints(rs);
				break;
             // case 4: for check total customers details
			case "5":
				ArrayList<Registration> registration_list = adminServicesImpl.totalCustomers();
				printCustomers(registration_list);
				break;
            // case 6: for see total complaints
			case "6":
				adminServicesImpl.totalComplaints();
				checkComplaintToUpdate();
				break;
			//case  7: for exit
			case "7":
				System.out.println("....Thank You....");
				System.out.println("");
				flag = false;
				break;
            // default case
			default:
				System.out.println("Invalid Option!...");
				break;

			}
		}

	}// admin_services
    /**
     * method for update the complaint status by using three ways
     * by using reference Id
     * bu using Consumer id 
     * by using service id
     */
	public void checkComplaintToUpdate() {

		AdminServicesIMPL adminServicesImpl = new AdminServicesIMPL();

		System.out.println("---Enter the option---");
		System.out.println("");
		System.out.println("   1.By ReferenceId");
		System.out.println("   2.By ConsumerId ");
		System.out.println("   3.By ServiceId");
		
		String choice = sc.next();
		/*
		 * switch case, it will check based on the respective id and set the complpaint status 
		 */
		switch (choice) {
		case "1":
			System.out.println("Enter the Reference ID of the Complaint..");
			int reference_id = sc.nextInt();
			Complaints complaints = adminServicesImpl.checkByReferenceId(reference_id);
			setStatus(complaints);
			break;

		case "2":
			System.out.println("Enter the Consumer ID To fetch the Complaints..");
			String consumer_id = sc.next();
			ArrayList<Complaints> rs = adminServicesImpl.checkByConsumerId(consumer_id);
			printComplaints(rs);
            
			System.out.println("");
			System.out.println("Enter the Reference ID from above Complaints Table..");
			int reference_id1 = sc.nextInt();

			Complaints complaints1 = adminServicesImpl.checkByReferenceId(reference_id1);
			setStatus(complaints1);
			break;

		case "3":
			System.out.println("Enter the Service ID To fetch the Complaints..");
			String service_id = sc.next();
			ArrayList<Complaints> rs1 = adminServicesImpl.checkByServiceId(service_id);
			printComplaints(rs1);
            
			System.out.println("");
			System.out.println("Enter the Reference ID from above Complaints Table..");
			int reference_id2 = sc.nextInt();

			Complaints complaints2 = adminServicesImpl.checkByReferenceId(reference_id2);
			setStatus(complaints2);
			break;

		default:
			System.out.println("You Selected an Invalid option..!");
		}
	}
    /**
     * method for print all the complaints
     * @param complaints_list
     */
	public void printComplaints(ArrayList<Complaints> complaints_list) {

		// Complaints complaints =null;
		System.out.println(
				"                                            Total complaints                                         ");
		System.out.println(
				"                                            ----------------                                         ");
		System.out.println(
				"Complaint ReferenceId\tComplaint Id\tConsumer Id\tService Id\tComplaint Name\t\tDate Of Complaint");
		System.out.println(
				"---------------------\t------------\t-----------\t----------\t--------------\t\t-----------------");

		Iterator<Complaints> itr = complaints_list.iterator();

		while (itr.hasNext()) {
			Complaints complaints = itr.next();
			System.out.println(complaints.getReference_Id() + "\t\t\t" + complaints.getComplaint_id() + "\t\t "
					+ complaints.getConsumer_Id() + "\t\t " + complaints.getService_Id() + "\t\t "
					+ complaints.getComplaint_Name() + "\t\t " + complaints.getDate_of_Complaint());
		}
	}
    /**
     * method for print customers who are registered
     * @param registration_list
     */
	public void printCustomers(ArrayList<Registration> registration_list) {

		System.out.println("      Existed Customers    ");
		System.out.println("      -----------------    ");
		System.out.println("ID\tPassword\tAddhar");
		System.out.println("--\t--------\t------");

		Iterator<Registration> itr = registration_list.iterator();

		while (itr.hasNext()) {

			Registration register = itr.next();
			System.out.println(register.getCus_id() + " \t" + register.getPwd() + "\t\t " + register.getAadhar_num());
		}

	}
     /**
      * method for set the complaint status
      * @param complaints
      */
	public void setStatus(Complaints complaints) {

		AdminServicesIMPL adminServicesImpl = new AdminServicesIMPL();

		System.out.println(
				"Complaint ReferenceId\tComplaint Id\tConsumer Id\tService Id\tComplaint Name\t\tDate Of Complaint");

		System.out.println(
				"---------------------\t------------\t-----------\t----------\t--------------\t\t-----------------");

		System.out.println(complaints.getReference_Id() + "\t\t\t" + complaints.getComplaint_id() + "\t\t "
				+ complaints.getConsumer_Id() + "\t\t " + complaints.getService_Id() + "\t\t "
				+ complaints.getComplaint_Name() + "\t\t " + complaints.getDate_of_Complaint());

		System.out.println("choose option: 1.Update Complaint Status 2. Redirect");
		String choice = sc.next();

		switch (choice) {
		case "1":
			System.out.println("Enter the vlaue to Update the Status");
			String status = sc.next();

			System.out.println("Enter the comment");
			String comment = sc.next();

			boolean flag = false;
			String date = null;
			Date newdate = null;

			while (!flag) {

				System.out.println("Enter the Expected Resolving Date");
				date = sc.next();
				flag = dateOfResolving(DATE_PATTERN, date);

				if (flag) {

					try {
						newdate = new SimpleDateFormat("yyyy/MM/dd").parse(date);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			}

			int reference_id = complaints.getReference_Id();
			String consumer_id = complaints.getConsumer_Id();

			adminServicesImpl.setComplaintStatus(reference_id, consumer_id, status, newdate, comment);
			
			break;
		case "2":
			adminServices();
			break;
		default:
			System.out.println("Invalid Option!...");

		}
	}

	public boolean dateOfResolving(String DATE_PATTERN, String date) {

		Pattern pattern;
		Matcher matcher;
		pattern = Pattern.compile(DATE_PATTERN);
		matcher = pattern.matcher(date);
		if (matcher.matches()) {

			matcher.reset();

			if (matcher.find()) {

				String day = matcher.group(1);
				String month = matcher.group(2);
				int year = Integer.parseInt(matcher.group(3));

				if (day.equals("31") && (month.equals("4") || month.equals("6") || month.equals("9")
						|| month.equals("11") || month.equals("04") || month.equals("06") || month.equals("09"))) {
					return false; // only 1,3,5,7,8,10,12 has 31 days
				} else if (month.equals("2") || month.equals("02")) {
					// leap year
					if (year % 4 == 0) {
						if (day.equals("30") || day.equals("31")) {
							return false;
						} else {
							return true;
						}
					} else {
						if (day.equals("29") || day.equals("30") || day.equals("31")) {
							return false;
						} else {
							return true;
						}
					}
				} else {
					return true;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}

	}

}
