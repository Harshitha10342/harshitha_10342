package com.consumercomplaints.bean;
/**
 *pojo class for new registration
 * @author IMVIZAG
 *
 */
public class Registration {
	String cus_id;
	String pwd;
	String aadhar_num;
	
	// getter and setter methods
	public String getCus_id() {
		return cus_id;
	}

	public void setCus_id(String cus_id) {
		this.cus_id = cus_id;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getAadhar_num() {
		return aadhar_num;
	}

	public void setAadhar_num(String aadhar_num) {
		this.aadhar_num = aadhar_num;
	}
}
