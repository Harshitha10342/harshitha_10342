package com.consumercomplaints.services;

import java.util.ArrayList;

import java.util.Map;

import com.consumercomplaints.bean.Complaints;
import com.consumercomplaints.bean.Registration;
import com.consumercomplaints.bean.UpdatedComplaints;
import com.consumercomplaints.dao.UserDAOImpl;
/**
 * Implementation class for UserServices
 * All method implementaion is provided in this class
 * @author IMVIZAG
 *
 */
public class UserServicesIMPL implements UserServices{
    
	UserDAOImpl userDAOIMPL = new UserDAOImpl();
	
	public Map<Integer, String> fetchAllServices() {
		
		Map<Integer, String> user_map = userDAOIMPL.getAllServices();
		return user_map;
	}
	/**
	 * method for displaying the user services
	 */
	public boolean setComplaint(int service_Id, String complaint_Name,int complaint_id,String user_id) {
		
		java.util.Date date=new java.util.Date();  
			
		Complaints complaints = new Complaints();
		
//		complaints.setReference_Id(reference_Id);
		complaints.setComplaint_Name(complaint_Name);
		complaints.setComplaint_id(complaint_id);
		complaints.setConsumer_Id(user_id);
		complaints.setService_Id(service_Id);
		complaints.setDate_of_Complaint(date);
		
		boolean complaintMade = userDAOIMPL.setCompalaints(complaints);
		
		return complaintMade;
	}
    /**
     * method for new user registration
     */
	public boolean newUserRegistration(Registration register) {
		
		boolean result = userDAOIMPL.createUserRegistration(register);		
		return result;
	}
   /**
    * method for finding Id
    */
	public boolean findById(String id){
		
		boolean flag = userDAOIMPL.searchById(id);	
		return flag;
	}
	/**
	 * method for validate the user id while register
	 * @param id
	 * @return
	 */
	public boolean validateId(String id){
		
		boolean flag = findById(id);
		return flag;
	}
    /**
     * method for checking the login person is admin or user
     */
	public String checkPerson() {
		
		String person = userDAOIMPL.checkAdminOrUser();
		return person;	
	}
	/**
	 * method for login
	 */
	public boolean login(String id, String password) {
		
		boolean flag =userDAOIMPL.login(id, password);		
		return flag;
	}
	/**
	 * method fro validate the Addhar while Register
	 */
	public boolean validateAadhar(String aadhar) {
		
		boolean result = userDAOIMPL.validateAadhar(aadhar);
		return result;
	}
	/**
	 * method for updating the complaint status
	 */
	public  ArrayList<UpdatedComplaints> myUpdatedComplaintStatus(String id){	
		
		ArrayList<UpdatedComplaints> complaint_list = userDAOIMPL.myUpdatedComplaint(id);	
		return complaint_list;
	}
	/**
	 * method for update the user password
	 */
	public boolean updatingMyPassword(String consumer_id, String password) {
		boolean isUpdated = userDAOIMPL.updateMyPassword(consumer_id,password);
		return isUpdated;
		
	}
	
}
