package com.consumer.dao;

import java.util.ArrayList;
import java.util.List;

import com.consumer.entity.Complaints;
import com.consumer.entity.Registration;


/**
 * interface for Admin services
 * this class contains methods for admin operation
 * @author IMVIZAG
 *
 */
public interface AdminDAO {
	
	boolean addExtraService(String service);
	boolean removeService(int key);
	boolean modifyService(int key, String update_service);
	List<Complaints> allComplaints();
    ArrayList<Registration> allCustomers();

}
