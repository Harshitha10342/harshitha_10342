package com.MobileBillNew.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.MobileBillNew.bean.Adminplans;
import com.MobileBillNew.service.AdminPlansService;
import com.MobileBillNew.service.BankAccount;

/**
 * This class is used for Testing of admin plans crud operations and Bank
 * account
 * 
 * @author IMVIZAG
 *
 */
public class AdminPlansTest {

	ArrayList<Adminplans> list = null;

	AdminPlansService service = null;

	/**
	 * TestCase method for creating the AdminService object It executes before each
	 * and every @Test(annotation) method
	 */
	@Before
	public void setUp() {
		service = new AdminPlansService();
		list = new ArrayList<Adminplans>();

	}

	/**
	 * TestCase method for storing null in the AdminService object It executes after
	 * each and every @Test(annotation) method
	 */
	@After
	public void tearDown() {

		service = null;
		list = null;
	}

	/**
	 * Testcase method for displaying the plans in positive case
	 */
	@Ignore
	@Test
	public void doDisplayPlansTestTrueCase() {

		list = service.doDisplayPlans(1);
		assertNotNull(list.size());
	}

	/**
	 * Testcase method for displaying the plans in negative case
	 */
	@Ignore
	@Test
	public void doDisplayPlansTestNegativeCase() {

		list = service.doDisplayPlans(1);
		assertEquals(0, list.size());
	}

	/**
	 * Testcase method for adding the plans in positive case
	 */
	@Ignore
	@Test
	public void doAddingPlansTestTrueCase() {
		Adminplans ap = new Adminplans();
		ap.setPlan_name("RC299");
		ap.setData("unlimited pack");
		ap.setAmazon_prime("28");
		ap.setAmount(299);
		ap.setCalls(100);
		ap.setNetfix("28");
		ap.setValidity(28);
		Assert.assertTrue(service.doAddingPlans(ap, 3));

	}

	/**
	 * Testcase method for adding the plans in negative case
	 */
	@Ignore
	@Test
	public void doAddingPlansTestNegativeCase() {
		Adminplans ap = new Adminplans();
		ap.setPlan_name("RC199");
		ap.setData("unlimited pack");
		ap.setAmazon_prime("28");
		ap.setAmount(199);
		ap.setCalls(100);
		ap.setNetfix("28");
		ap.setValidity(28);
		assertFalse(service.doAddingPlans(ap, 3));

	}

	/**
	 * Testcase method for deleting the plans in positive case
	 */
	@Ignore
	@Test
	public void doDeletePlansTestTrueCase() {
		String planName = "RC299";
		// Unlimited prepaid plans
		int planType = 3;
		boolean result = service.doDeletePlans(planType, planName);
		assertTrue(result);

	}

	/**
	 * Testcase method for deleting the plans in negative case
	 */
	@Ignore
	@Test
	public void doDeletePlansTestNegativeCase() {
		String planName = "RC199";

		int planType = 3;
		boolean result = service.doDeletePlans(planType, planName);
		assertFalse(result);

	}

	/**
	 * Testcase method for getting balance from account using google pay in positive
	 * case
	 */

	@Ignore
	@Test
	public void doGetGooglePayBalanceTrueCase() {
		int balance = 7000;
		// String accountType = "googlepay";
		int amount = BankAccount.doGetBalance(balance);
		Assert.assertNotNull(amount);
	}

	/**
	 * Testcase method for getting balance from account using google pay in positive
	 * case
	 */

	@Ignore
	@Test
	public void doGetGooglePayBalanceNegativeCase() {
		int balance = 7000;
		// String accountType = "googlepay";
		int amount = BankAccount.doGetBalance(balance);
		Assert.assertNull(amount);
	}

	/**
	 * Testcase method to cut the balance from user account using google pay in
	 * positive case
	 */

	@Ignore
	@Test
	public void doGooglePaySendBalanceTestTrueCase() {
		int amount = 7800;
		int accountNumber = 13456;
		// String accountType = "googlepay";
		boolean result = BankAccount.doSendBalance(amount, accountNumber);
		Assert.assertTrue(result);

	}

	/**
	 * Testcase method to cut the balance from user account using google pay in
	 * negative case
	 */

	@Ignore
	@Test
	public void doGooglePaySendBalanceTestFalseCase() {
		int amount = 7800;
		int accountNumber = 13456;
		// String accountType = "googlepay";
		boolean result = BankAccount.doSendBalance(amount, accountNumber);
		Assert.assertFalse(result);

	}

	/**
	 * Testcase method for getting balance from account using paytm in positive case
	 */
	@Ignore
	@Test
	public void doGePaytmPayBalanceTrueCase() {
		int balance = 8000;
		// String accountType = "paytm";
		int amount = BankAccount.doGetBalance(balance);
		Assert.assertNotNull(amount);
	}

	/**
	 * Testcase method for getting balance from account using paytm in negative case
	 */
	@Ignore
	@Test
	public void doGePaytmPayBalanceNegativeCase() {
		int balance = 8000;
		// String accountType = "paytm";
		int amount = BankAccount.doGetBalance(balance);
		Assert.assertNull(amount);
	}

	/**
	 * Testcase method to cut the balance from user account using paytm in positive
	 * case
	 */
	@Ignore
	@Test
	public void doSendBalancePaytmTestTrueCase() {
		int amount = 7800;
		int accountNumber = 9949730;
		// String accountType = "paytm";
		boolean result = BankAccount.doSendBalance(amount, accountNumber);
		Assert.assertTrue(result);

	}

	/**
	 * Testcase method to cut the balance from user account using paytm in negative
	 * case
	 */
	@Ignore
	@Test
	public void doSendBalancePaytmTestNegativeCase() {
		int amount = 7800;
		int accountNumber = 9949730;
		// String accountType = "paytm";
		boolean result = BankAccount.doSendBalance(amount, accountNumber);
		Assert.assertFalse(result);

	}

	/**
	 * TestCase method for adding amount to admin account in positive case
	 */
	@Ignore
	@Test
	public void addingMoneytoadminAccountTestTrueCase() {
		int amount = 8000;
		int accountNumber = 62253522;
		boolean result = BankAccount.addingMoneytoadminAccount(amount, accountNumber);
		Assert.assertTrue(result);

	}

	/**
	 * TestCase method for adding amount to admin account in negative case
	 */
	@Ignore
	@Test
	public void addingMoneytoadminAccountTestNegativeCase() {
		int amount = 8000;
		int accountNumber = 62253522;
		boolean result = BankAccount.addingMoneytoadminAccount(amount, accountNumber);
		Assert.assertFalse(result);

	}
}
