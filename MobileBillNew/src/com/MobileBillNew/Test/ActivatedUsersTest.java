package com.MobileBillNew.test;

import java.sql.Date;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.MobileBillNew.bean.Adminplans;
import com.MobileBillNew.service.ActivatedUsers;

/**
 * Class for testing the methods
 */
public class ActivatedUsersTest {

	ActivatedUsers user = null;

	/**
	 * TestCase method for creating the AdminService object It executes before each
	 * and every @Test(annotation) method
	 */
	@Before
	public void setUp() {
		user = new ActivatedUsers();
	}

	 /**
	  * TestCase method for storing null in the AdminService object It executes after
	  * each and every @Test(annotation) method
	  */
	@After
	public void tearDown() {
		user = null;
	}

	/**
	 * this method is used to test plan activation in positive case
	 */
	@Test
	public void plansActivationTest() {
		Adminplans plans = new Adminplans();
		plans.setAmazon_prime("28days");
		plans.setAmount(199);
		plans.setCalls(100);
		plans.setData("unlimited");
		plans.setNetfix("28days");
		plans.setPackType("special plan");
		plans.setPlan_name("RC199");
		plans.setValidity(28);
		Date activation_date = new Date(2019 - 01 - 22);
		Date expiry_date = new Date(2019 - 02 - 21);
		boolean activation = ActivatedUsers.plansActivation(plans, activation_date, expiry_date, 1);
		Assert.assertTrue(activation);
	}

	/**
	 * this method is used to test plan activation in negative case
	 */
	@Test
	public void plansActivationNegativeTest() {
		Adminplans plans = new Adminplans();
		plans.setAmazon_prime("28days");
		plans.setAmount(199);
		plans.setCalls(100);
		plans.setData("unlimited");
		plans.setNetfix("28days");
		plans.setPackType("special plan");
		plans.setPlan_name("RC199");
		plans.setValidity(28);
		Date activation_date = new Date(2019 - 01 - 22);
		Date expiry_date = new Date(2019 - 02 - 21);
		boolean activation = ActivatedUsers.plansActivation(plans, activation_date, expiry_date, 0);
		Assert.assertFalse(activation);
	}

	/**
	 * this method is used to test the user id
	 */
	@Test
	public void getUserIdPositiveTest() {
		int user_id = ActivatedUsers.getUsersId(1);
		Assert.assertNotNull(user_id);
	}

	/**
	 * this method is used to test the user id in negative case
	 */
	@Test
	public void getUserIdNegativeTest() {
		int user_id = ActivatedUsers.getUsersId(0);
		Assert.assertEquals(0, user_id);
	}
	
	/**
	 * this method is used to test the activation id
	 */
	@Test
	public void getActivationIdTest() {
		int activation_id = ActivatedUsers.getActivationId(1);
		Assert.assertNotNull(activation_id);
	}

	/**
	 * this method is used to test the activation id in negative case
	 */
	@Test
	public void getActivationIdNegativeTest() {
		int activation_id = ActivatedUsers.getActivationId(0);
		Assert.assertEquals(0, activation_id);
	}
}
