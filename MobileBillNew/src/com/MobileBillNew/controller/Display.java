package com.MobileBillNew.controller;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.MobileBillNew.bean.Account;
import com.MobileBillNew.service.AccountService;
import com.MobileBillNew.service.ActivatedUsers;
import com.MobileBillNew.service.PostpaidBill;
import com.MobileBillNew.service.UserService;
/**
 * this class is used to display details when a user and admin logs in
 * @author IMVIZAG
 *
 */
public class Display {

	static Scanner sc = new Scanner(System.in);

	/**
	 * this method displays the details to users whenever they log into the application
	 * @param mobilenumber
	 * @param network
	 * @param service_id
	 */
	public static void UserDisplay(String mobilenumber, String network, int service_id) {

		System.out.println("1.Plans\n2.do you want to switch?\n3.logout");
		int choice = sc.nextInt();
		switch (choice) {
		case 1:
			if (network.equals("prepaid")) {
				Plans.display_PrepaidPlans(service_id);
			} else if (network.equals("postpaid")) {
				int activation_id = ActivatedUsers.getActivationId(service_id);
				if (activation_id != 0) {
					Bill.billCalculation(activation_id);
				} else {
					Plans.display_PostpaidPlans(service_id);
				}
			}
			break;
		case 2:
			System.out.println("Are you sure you want to switch your account?(press yes/no)");
			String switch_option = sc.next();
			if (switch_option.equalsIgnoreCase("yes")) {
				if (AccountService.setUserSwitchRequest(mobilenumber, network)) {
					System.out.println(
							"Thank you,User\nYour request has been recorded we'll get back to you with in 2-3 working days");
				} else {
					System.out.println("Sorry User,technical error occurred ");
				}
			} else {
				System.out.println("Thank you");
			}
			break;
		case 3:
			System.out.println("Thank you");
			System.exit(0);
			break;
		default:
			System.out.println("Invalid Input");
		}

	}

	/**
	 * This method is used to display the details whenever a admin logs into the system
	 */
	public static void adminDisplay() {

		System.out.println("1.plans\n2.Display Postpaid Bills\n3.Account Switch Request\n4.Logout");
		int choice = sc.nextInt();
		switch (choice) {

		case 1:
			AdminPlans.plans();
			break;
		case 2:
			List<com.MobileBillNew.bean.Bill> billList = PostpaidBill.displayUsers();
			System.out.println("                 Name            MobileNumber             Bill             ExpiryDate");
			for (int i = 0; i < billList.size(); i++) {
				System.out.println(i + 1 + ".                " + billList.get(i).getName() + "              "
						+ billList.get(i).getMobile_number() + "            " + billList.get(i).getAmount()
						+ "             " + billList.get(i).getExpiry_date());
			}
			Date date = new Date(new java.util.Date().getTime());
			System.out.println("Today's date is:" + date);
			System.out.println("1.Choose a user to block their number\n2.Go Back\n3.Logout");
			int select_option = sc.nextInt();
			if(select_option==1){
				System.out.println("Choose the user based on their serial no:");
				int select = sc.nextInt();
				if(select <= billList.size()) {
					
					if(UserService.blockUser(billList.get(select-1).getMobile_number())) {
					System.out.println("Blocked");
					}
					else {
						System.out.println("Technical Problem");
					}
				}
				
			}
			else if(select_option == 2) {
				
				adminDisplay();
			}
			else if(select_option == 3) {
				System.out.println("Thank you");
				System.exit(0);
			}
			else
			{
				System.out.println("Invalid Input");
			}
			break;
		case 3:
			System.out.println("The Users Requested for Account Switch are/is:");
			List<Account> list = new ArrayList<Account>();
			list = AccountService.fetchSwitchAccountData();
			System.out.println("          MobileNumber                    User Network                       Status");
			for (int i = 0; i < list.size(); i++) {
				System.out.println(i + 1 + ".          " + list.get(i).getMobileNumber() + "                         "
						+ list.get(i).getNetwork() + "                      " + list.get(i).getStatus());
			}
			System.out.println("1.select a user to approve or deny\n0.Go Back");
			int selection = sc.nextInt();
			if (selection == 1) {
				System.out.println("choose the user(based on serial no):");
				int switchuser = sc.nextInt();
				if (switchuser <= list.size()) {
					System.out.println("set the status of the user(Approved/Denied)");
					String status = sc.next();

					AccountService.updateDetails(list.get(switchuser - 1).getMobileNumber(),
							list.get(switchuser - 1).getNetwork(), status);

					System.out.println("0.Go Back 1.Logout");
					int option = sc.nextInt();
					if (option == 0) {
						adminDisplay();
					} else if (option == 1) {
						System.out.println("Thank you");
						System.exit(0);
					} else {
						System.out.println("Invalid Input");
					}
				}
			} else if (selection == 0) {
				adminDisplay();
			}
			break;
		case 4:
			System.out.println("Thank you");
			System.exit(0);
			break;
		default:
			System.out.println("Invalid Input");
		}

	}

}
