package com.MobileBillNew.controller;

import java.sql.Date;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.MobileBillNew.service.UserService;

/**
 * this class is makes a user to login into the application
 * 
 * @author IMVIZAG
 *
 */
public class UserLogin {

	static final String VALIDATION_PATTERN = "(0/91)?[7-9][0-9]{9}";
	public static final String PREPAID_STRING = "prepaid";
	public static final String POSTPAID_STRING = "postpaid";

	/**
	 * this method enables a user to login into the system
	 */
	public static void doLogin() {

		// creates an instance for scanner class
		Scanner sc = new Scanner(System.in);
		int attemptsCount = 0;

		attemptsloop: while (attemptsCount < 4) {
			System.out.println("-------------------------------------------------------------------------------------------------------------");

			System.out.println("Enter your Mobile Number:");
			String mobileNumberInput = sc.next();

			// validates the given mobile number
			if (mobileNumberValidation(mobileNumberInput)) {

				// this function return the service_id
				int service_id = UserService.validateUser(mobileNumberInput);
				// System.out.println(service_id);
				if (service_id != 0) {

					// checks whether the given number is already registered or not
					if (UserService.registeredUser(mobileNumberInput)) {

						// gets the network of the user
						String network = UserService.networkOfUser(mobileNumberInput);

						// based on the network it displays the plans
						switch (network) {
						case PREPAID_STRING:
						case POSTPAID_STRING:

							// generates the otp for valid user
							if (OtpGeneration.otpGen()) {
								System.out.println("-------------------------------------------------------------------------------------------------------------");

								System.out.println("Login successful");
								System.out.println("Mobile Number = " + mobileNumberInput);
								System.out.println("-------------------------------------------------------------------------------------------------------------");
								Display.UserDisplay(mobileNumberInput, network,service_id);

							} else {
								System.out.println("Login failed, due to wrong entry of otps...please try again!!");
							}
							break;
						case "blocked":
							System.out.println("your number is blocked");
						default:
							System.out.println();
							break;
						}
					}

					// if a user is not registered
					else {
						System.out.println("user was not registered,please register with ur mobile number again");
						// System.out.println("Your Number Doesn't exists,Please Register with number");
						System.out.println("Enter your name");
						String name = sc.next();
						System.out.println("Enter your gender(male/female)");
						String gender = sc.next();
						System.out.println("Enter your Date of birth(yyyy-mm-dd)");
						String date = sc.next();
						Date dob = Date.valueOf(date);
						if (UserService.userRegistration(mobileNumberInput, name, gender, dob, service_id)) {
							System.out.println("Registered Successfully");

							String network = UserService.networkOfUser(mobileNumberInput);

							// based on the network it displays the plans
							switch (network) {
							case PREPAID_STRING:
							case POSTPAID_STRING:

								// generates the otp for valid user
								if (OtpGeneration.otpGen()) {
									System.out.println("Login successful");
									System.out.println("Mobile Number = " + mobileNumberInput);
									Display.UserDisplay(mobileNumberInput, network,service_id);

								} else {
									System.out.println("Login failed, due to wrong entry of otps...please try again!!");
								}
								break;
							default:
								System.out.println();
								break;
							}

						} else {
							System.out.println("Your not Registered");
						}
					}
					break attemptsloop;
				} else {
					System.out.println("Your Number Doesn't belongs to our Network,try with valid number");
					System.exit(0);
				}
			} else {

				attemptsCount++;
				if (attemptsCount != 4) {
					System.out.println("the entered number is wrong,please renter. Attempt no." + attemptsCount);
				}
			}

		}

		if (attemptsCount == 4)

		{
			System.out.println("Too many attempts, please try after some time");
			System.exit(1);

			// TODO next steps here
		}
		// closes scanner class
		sc.close();
	}

	/**
	 * this function is used to validate the mobile number
	 * 
	 * @param mobileNumber mobile number of an user
	 * @return true if number is valid,false if number is not valid
	 */
	static boolean mobileNumberValidation(String mobileNumber) {
		// creating a matcher to validate the input number
		Matcher m = Pattern.compile(VALIDATION_PATTERN).matcher(mobileNumber);

		// return true if the match is successful, else false
		return (m.find() && m.group().equals(mobileNumber));
	}
	
	/**
	 * this method is used to logout the user
	 */
	public static void doLogout()
	{
		Scanner sc = new Scanner(System.in);
		System.out.println("1.Go Home \n2.Logout");
		int option = sc.nextInt();
		switch(option) {
		case 1:Main.main(null);
		break;
		case 2:System.exit(0);
		break;
		default:System.out.println("Invalid input");
		}
		sc.close();
	}
}
