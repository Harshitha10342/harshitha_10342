package com.MobileBillNew.controller;

import java.sql.Date;
import java.util.Calendar;

import com.MobileBillNew.service.PostpaidBill;

/**
 * this is used to generate bill for postpaid users
 * @author IMVIZAG
 *
 */
public class Bill {

	/**
	 * this method is used to calculate the bill
	 * @param activation_id
	 */
	public static void billCalculation(int activation_id) {

		// sets expiry date
		Date expiry_date = PostpaidBill.getExpiryDate(activation_id);
		// System.out.println(expiry_date);
		Calendar cal = Calendar.getInstance();
		cal.setTime(expiry_date);

		// sets todays date
		Date date = new Date(new java.util.Date().getTime());
		// System.out.println(date);
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		float bill = 0.0f;

		// sets date to block to a user
		Calendar cl = Calendar.getInstance();
		cl.setTime(expiry_date);
		cl.add(Calendar.DATE, 3);
		//System.out.println(cl.getTime());

		if (cal.get(Calendar.MONTH) == c.get(Calendar.MONTH) && cal.get(Calendar.YEAR) == c.get(Calendar.YEAR)
				&& cal.get(Calendar.DAY_OF_MONTH) == c.get(Calendar.DAY_OF_MONTH)) {

			
			String dataUsed = PostpaidBill.dataUsed(activation_id);
			// System.out.println(dataUsed);
			int plan_id = PostpaidBill.planId(activation_id);
			// System.out.println(plan_id);
			String actualData = PostpaidBill.actualData(plan_id);
			// System.out.println(actualData);
			if (actualData.equalsIgnoreCase(dataUsed)) {
				System.out.println("Your Bill is:");
				bill = PostpaidBill.getAmount(plan_id);
				if (PostpaidBill.setDataIntoTable(activation_id, dataUsed, bill)) {
					System.out.println(bill);
				} else {
					System.out.println("sorry technical problem");
				}

			} else if (!actualData.equalsIgnoreCase(dataUsed)) {
				System.out.println("consumed more data");
				bill = PostpaidBill.getAmount(plan_id);
				float totalBill = PostpaidBill.getPriceFromDataUsage();
				totalBill = totalBill + bill + 100;
				if (PostpaidBill.setDataIntoTable(activation_id, dataUsed, bill)) {
					System.out.println("Your Bill is:" + totalBill);
				} else {
					System.out.println("sorry technical problem");
				}

			} else {
				System.out.println("your data will be added to next month");
				bill = PostpaidBill.getAmount(plan_id);
				if (PostpaidBill.setDataIntoTable(activation_id, dataUsed, bill)) {
					System.out.println(bill);
				} else {
					System.out.println("sorry technical problem");
				}
			}
		} else if (cl.get(Calendar.MONTH) == c.get(Calendar.MONTH) && cl.get(Calendar.YEAR) == c.get(Calendar.YEAR)
				&& cl.get(Calendar.DAY_OF_MONTH) == c.get(Calendar.DAY_OF_MONTH)) {

			System.out.println("Your Number is Blocked");


		} else if (cl.get(Calendar.MONTH) == cal.get(Calendar.MONTH) && cl.get(Calendar.YEAR) == cal.get(Calendar.YEAR)
				|| cl.get(Calendar.DAY_OF_MONTH) == cal.get(Calendar.DAY_OF_MONTH)) {
	
//			System.out.println("Pay the bill on or before "+cl.get(Calendar.DATE)+"/"+cl.get(Calendar.MONTH)+1 +"/"+cl.get(Calendar.YEAR));
			if(expiry_date.after(date)) {
				System.out.println("Your Number is Blocked");
			}
			else {
				System.out.println("Pay the bill on or before "+cl.get(Calendar.DATE)+"/"+cl.get(Calendar.MONTH)+1 +"/"+cl.get(Calendar.YEAR));
			}
		}

		else {
			System.out.println("Your Plan is not Expired");
		}
	}

}
