package com.MobileBillNew.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import com.MobileBillNew.bean.Admin;
import com.MobileBillNew.service.AdminService;
import com.MobileBillNew.service.AdminServiceImpl;

/**
 * This class enables the Admin to login, register and to display the Admin
 * 
 * @author RANJHS
 *
 */

public class AdminRegistration {

	static Scanner sc = new Scanner(System.in);
	static Admin admin = new Admin();
	static AdminService adminservice = new AdminServiceImpl();
	static AdminServiceImpl as = new AdminServiceImpl();
	static int count = 0;

	/**
	 * This method helps the admins to login and register into the application
	 */
	static int countChoice=0;
	public static void adminRegistration() {

		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter your choice");
		System.out.println();
		System.out.println("1.Admin Login \n2.Admin Registration \n3.Display \n0.GoBack");
		int choice=0;
		
		try {
			choice = sc.nextInt();
		}catch(Exception e)
		{
			if(countChoice == 2 )
			{		System.out.println("-------------------------------------------------------------------------------------------------------------");
					
					System.out.println("You have entered wrong input so many times please try after sometime!!");
					System.exit(0);
			}
			countChoice++;
			System.out.println("input is invalid");
			System.out.println("-------------------------------------------------------------------------------------------------------------");
			
			adminRegistration();
		}
	
		
		switch (choice) {
		case 0:
			System.out.println("-------------------------------------------------------------------------------------------------------------");
			
			Main.mainDisplay();
			
			break;
		case 1:
			System.out.println("-------------------------------------------------------------------------------------------------------------");
			
			System.out.println("Enter the username:");
			String username1 = sc.next();
			boolean login1 = as.doLogin(username1);
			if (login1) {
				System.out.println("Enter the Password:");
				String password1 = sc.next();
				boolean login2 = as.doLogin(username1, password1);
				if (login2) {
					System.out.println("Successfull Login");
//					AdminPlans.plans();
					Display.adminDisplay();
				} else {
					System.out.println("Incorrect Password");
					System.out.println("Do you want to reset your password\nyes or no");
					String reset = sc.next();
					if (reset.equals("yes")) {
						System.out.println("Who is your Favourite Actor:");
						String securityanswer = sc.next();
						boolean secureQuestion=as.doSecurityanswer(securityanswer,username1);
						if(secureQuestion)
						{
							
						}
						reEnterDetails(username1);
					} else {
						System.out.println("Incorrect details");
						System.out.println("Exit");
						System.exit(0);
					}
				}

			} else {

				System.out.println("you are not registered");
				System.out.println("If you want register enter yes else no");
				String choiceRegister = sc.next();
				if (choiceRegister.equals("yes")) {
					registerUsers();
				} else {
					System.out.println("Exit");
					System.exit(0);
				}
			}
			break;
		case 2:
			registerUsers();
			break;// end of switch case break;

		case 3:
			System.out.println("-------------------------------------------------------------------------------------------------------------");

			System.out.println("UserName \t\t Password");
			List<Admin> list = new ArrayList<Admin>();
			list = adminservice.FetchAdmin();
			Iterator<Admin> iterator = list.iterator();
			System.out.println("-------------------------------------------------------------------------------------------------------------");
			while (iterator.hasNext()) {
				Admin it = iterator.next();
				System.out.printf("%-24s %-24s\n",it.getUserName(),it.getPassword());
				//System.out.println(it.getUserName() + " " + it.getPassword());
			}
			System.out.println("-------------------------------------------------------------------------------------------------------------");

			break;
			default:System.out.println("You entered incorrect credentials");
				adminRegistration();
			
		}

	}

	public static void registerUsers() {
		int countUsername = 0;
		int countPassword = 0;
		int countConfirmPassword = 0;
		System.out.println("-------------------------------------------------------------------------------------------------------------");
		
		while (true) {
			
			System.out.println("Enter the username:");
			String username = sc.next();

			boolean getResultusername = as.validateData(username);
			if (!getResultusername) {
				if (countUsername == 2) {
					System.out.println("Your entered wrong credential please try after some time");
					System.exit(0);
				}
				System.out.println("Your entered wrong credential please re enter username");

				countUsername++;
				continue;
			}
			admin.setUserName(username);
			while (true) {
				
				System.out.println("Enter the Password:");
				String password = sc.next();
				boolean getResultpassword = as.validateData(password);
				if (!getResultpassword) {
					if (countPassword == 2) {
						System.out.println("Your entered wrong credential please try after some time");
						System.exit(0);
					}
					System.out.println("Your entered wrong credential please re enter password");

					countPassword++;
					continue;
				}
				admin.setPassword(password);
				
				System.out.println("Enter confirm Password:");
				String confirmPassword = sc.next();
				if (!password.equals(confirmPassword)) {
					if (countConfirmPassword == 2) {
						System.out.println("Your entered wrong credential please try after some time");
						System.exit(0);
					}
					countConfirmPassword++;
					System.out.println("mis matched password Please enter again:");
					continue;
				}

				
				System.out.println("Who is your Favourite Actor:");
				String securityanswer = sc.next();
				admin.setSecurityAnswer(securityanswer);
				boolean result = adminservice.insertAdmin(admin);
				if (result) {
					System.out.println("Successfully Registered:");

				}
				break;// end of password while loop break
			}
			System.out.println("-------------------------------------------------------------------------------------------------------------");
			
			break;// end of while loop break;
		}
	}

	public static void reEnterDetails(String username) {

		System.out.println("Enter New Password:");
		String password2 = sc.next();
		System.out.println("Confirm Password");
		String password3 = sc.next();
		if (password2.equals(password3)) {
			@SuppressWarnings("unused")
			boolean result = as.updatePassword(username, password2);
			System.out.println("Successfully Reset password");
			// TODO LOGIN
		} else {
			if (count == 2) {
				System.out.println("Please try after sometime");
				System.exit(0);
			}
			System.out.println("passwords won't match");
			count++;
			reEnterDetails(username);

		}

	}
}
