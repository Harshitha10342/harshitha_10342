package com.MobileBillNew.bean;

/**
 * This Admin class is used to declare,set the values for the variables and
 * retrive them
 * 
 * @author RANJHS
 *
 */

public class Admin {
	// declaring the variables
	private String userName;
	private String password;
	private String securityQuestion;
	private String securityAnswer;

	// getter and setter methods
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSecurityQuestion() {
		return securityQuestion;
	}

	public void setSecurityQuestion(String securityQuestion) {
		this.securityQuestion = securityQuestion;
	}

	public String getSecurityAnswer() {
		return securityAnswer;
	}

	public void setSecurityAnswer(String securityAnswer) {
		this.securityAnswer = securityAnswer;
	}

}
