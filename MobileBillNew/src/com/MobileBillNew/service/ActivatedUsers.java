package com.MobileBillNew.service;

import java.sql.Date;

import com.MobileBillNew.bean.Adminplans;
import com.MobileBillNew.dao.ActivatedUsersDAO;

/**
 * This is the service package which calls the dao package
 * 
 * @author RANJHS
 *
 */

public class ActivatedUsers {
	
	public static boolean plansActivation(Adminplans plan, Date activation_date, Date expiry_date, int users_id) {

		int plan_id = ActivatedUsersDAO.getPlanId(plan);
		String dataused = null;
		int calls = 0;
		boolean res = ActivatedUsersDAO.plansActivation(plan_id, activation_date, expiry_date, users_id);
		int activations_id = ActivatedUsersDAO.getActivationId(users_id);
		boolean result =res && ActivatedUsersDAO.activatedUsers(plan_id, activations_id, dataused, calls);
		return result;
	}
	public static int getUsersId(int service_id) {
		int users_id = ActivatedUsersDAO.getUsersId(service_id);
		return users_id;
	}

	public static int getActivationId(int service_id) {
		int users_id = ActivatedUsersDAO.getUsersId(service_id);
		int activation_id = ActivatedUsersDAO.getActivationId(users_id);

		return activation_id;
	}

}