package com.MobileBillNew.service;

import com.MobileBillNew.dao.BankAccountDAO;

public class BankAccount {

	public static int doGetBalance(int balance)
	{
		int amount=BankAccountDAO.getAmount(balance);
		return amount;
	}
	public static boolean doSendBalance(int amount,int accountNumber)
	{
		boolean result= BankAccountDAO.doSendMoney(amount,accountNumber);
		return result;
	}
	public static void addingMoneytoadminAccount( int amount,int accountNumber)
	{
		BankAccountDAO.doAddingMoneyToAddmin(amount,accountNumber);
	}
}
