package com.MobileBillNew.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.MobileBillNew.util.DBUtil;

/**
 * this method is used to access the database
 * @author IMVIZAG
 *
 */
public class BankAccountDAO {

	static Connection con = null;
	static PreparedStatement ps = null;
	
	/**
	 * this method is used to get the amount from user or admin account
	 * @param accountNumber
	 * @return
	 */

	public static int getAmount(int accountNumber) {

		
		int getData=0;
		String sql = "select account_id from paytm where paytm_id=?";
		try {
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, accountNumber);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				int onlineAccountNumber=rs.getInt(1);				
				getData=getAccount(onlineAccountNumber);
				
			}

		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return getData;
	}
	/**
	 * this method is used to get the paytm account id
	 * @param account
	 * @return
	 */
	//this method used for Get account 
	public static int getAccount(int account)
	{
		int onlineAccountNumber=0;
		String sql = "select balance from Account where account_id=?";
		try {
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, account);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				onlineAccountNumber=rs.getInt(1);
						
				
			}

		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return onlineAccountNumber;
	}
	
	/**
	 * this method is used to get the account number
	 * @param accountNumber
	 * @return
	 */
	//this method used for get Account number 
	public static int getAccountNumber(int accountNumber) {

		int onlineAccountNumber=0;
		
		String sql = "select account_id from paytm where paytm_id=?";
		try {
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, accountNumber);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				onlineAccountNumber=rs.getInt(1);				
				
			}

		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return onlineAccountNumber;
	}
	/**
	 * this method is used to sets the balance
	 * @param amount
	 * @param accountNumber
	 * @return
	 */
	//this method used for deposite money from account
	public static boolean doSendMoney(int amount,int accountNumber)
	{
			
		int accountNumberAccount=getAccountNumber(accountNumber);
		
		boolean result=false;
		String sql = "update Account set balance = ? where account_id= ? ";
		try {
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, amount);
			ps.setInt(2,accountNumberAccount);
			int rs = ps.executeUpdate();
			if (rs > 0) {
				result = true;
			}
			else {
				System.out.println("failed");
				return false;
			}

		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}
	
	/**
	 * this method adds money to admin
	 * @param amount
	 * @param accountNumber
	 */
	public static void doAddingMoneyToAddmin(int amount,int accountNumber)
	{
			
		
		String sql = "update Account set balance = ? where account_id= ? ";
		try {
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, amount);
			ps.setInt(2,accountNumber);
			int rs = ps.executeUpdate();
			if (rs > 0) {
				//result = true;
			}
			else {
				System.out.println("failed");
				
			}

		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

}
