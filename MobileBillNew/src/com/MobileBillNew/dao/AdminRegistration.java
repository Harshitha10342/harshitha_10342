package com.MobileBillNew.dao;

import java.util.List;
import com.MobileBillNew.bean.Admin;

/**
 * this is the interface in which all the abstract methods are declared
 * 
 * @author RANJHS
 *
 */

public interface AdminRegistration {

	boolean createAdmin(Admin admin);

	List<Admin> displayAdmin();

}