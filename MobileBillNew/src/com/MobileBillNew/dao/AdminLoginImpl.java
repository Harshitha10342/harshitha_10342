package com.MobileBillNew.dao;

/**
 * this is the interface in which all the abstract methods are declared
 * 
 * @author RANJHS
 *
 */

public interface AdminLoginImpl {

	boolean doLogin(String username);

}


