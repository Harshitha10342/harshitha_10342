package com.MobileBillNew.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.MobileBillNew.bean.Adminplans;
import com.MobileBillNew.util.DBUtil;

/**
 * This class is used to insert the Activated Users data and gets the fields
 * from the database
 * 
 * @author IMVIZAG
 *
 */

public class ActivatedUsersDAO {

	/**
	 * This method retrieves the planid if the planname is equal
	 * 
	 * @param plan It retrieves the plan from the pojo class
	 * @return It returns the planid
	 */

	public static int getPlanId(Adminplans plan) {
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		int result = 0;
		String plan_name = plan.getPlan_name();
		try {
			con = DBUtil.getCon();
			String sql = "select plan_id,plan_name from plans where plan_name = plan_name";
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				if (plan_name.equals(rs.getString(2))) {
					result = rs.getInt(1);
				}
			}

		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				con.close();

			} catch (SQLException e) {

			}
		}
		// returns the result to the service package
		return result;
	}

	/**
	 * This method is used to insert the plans for the activated users
	 * 
	 * @param plan_id
	 * @param activation_date
	 * @param expiry_date
	 * @param users_id
	 * @return It returns the planid
	 */

	public static boolean plansActivation(int plan_id, Date activation_date, Date expiry_date, int users_id) {
		Connection con = null;
		PreparedStatement ps = null;
		boolean res = false;
		try {
			con = DBUtil.getCon();
			String sql = "insert into activated_users(plans_id,activation_date,expiry_date,users_id) values(?,?,?,?) ";
			ps = con.prepareStatement(sql);
			ps.setInt(1, plan_id);
			ps.setDate(2, activation_date);
			ps.setDate(3, expiry_date);
			ps.setInt(4, users_id);
			int result = ps.executeUpdate();
			if (result != 0) {
				res = true;
			}

		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				con.close();

			} catch (SQLException e) {

			}
		}
		// returns the result to the service package
		return res;
	}

	/**
	 * It inserts the activated users data into the database
	 * 
	 * @param plan_id
	 * @param activation_id
	 * @param dataused
	 * @param calls
	 * @return true or false based on result
	 */

	public static boolean activatedUsers(int plan_id, int activation_id, String dataused, int calls) {
		Connection con = null;
		PreparedStatement ps = null;
		boolean res = false;
		try {
			con = DBUtil.getCon();
			String sql = "insert into activated_users1(plan_id,activation_id,datausage,calls) values(?,?,?,?) ";
			ps = con.prepareStatement(sql);
			ps.setInt(1, plan_id);
			ps.setInt(2, activation_id);
			ps.setString(3, dataused);
			ps.setInt(4, calls);
			int result = ps.executeUpdate();
			if (result != 0) {
				res = true;
			}

		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				con.close();

			} catch (SQLException e) {

			}
		}
		// returns the result to the service package
		return res;
	}

	/**
	 * This method is used to get the userid based on serviceid
	 * 
	 * @param service_id
	 * @return It returns the serviceid
	 */

	public static int getUsersId(int service_id) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int result = 0;
		try {
			con = DBUtil.getCon();
			String sql = "select users_id,service_id from users where service_id = ? ";
			ps = con.prepareStatement(sql);
			ps.setInt(1, service_id);
			rs = ps.executeQuery();
			while (rs.next()) {
				if (service_id == rs.getInt(2)) {
					result = rs.getInt(1);
				}
			}

		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				con.close();

			} catch (SQLException e) {

			}
		}
		// returns the result to the service package
		return result;

	}

	/**
	 * This method is used to get the activationid based on userid
	 * 
	 * @param users_id
	 * @return It returns the userid
	 */

	public static int getActivationId(int users_id) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int result = 0;
		try {
			con = DBUtil.getCon();
			String sql = "select users_id,activation_id from activated_users where users_id = ? ";
			ps = con.prepareStatement(sql);
			ps.setInt(1, users_id);
			rs = ps.executeQuery();
			while (rs.next()) {
				if (users_id == rs.getInt(1)) {
					result = rs.getInt(2);
				}
			}

		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				con.close();

			} catch (SQLException e) {

			}
		}
		// returns the result to the service package
		return result;
	}

}
