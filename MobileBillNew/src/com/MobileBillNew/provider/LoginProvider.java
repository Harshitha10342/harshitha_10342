package com.MobileBillNew.provider;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.MobileBillNew.service.AdminServiceImpl;
import com.MobileBillNew.service.UserService;

/**
 * This is the resource class which acts as web resource which is specified with
 * a URL
 * 
 * @author RANJHS
 *
 */

@Path("/login")
public class LoginProvider {

	/**
	 * This is resource method which performs user login operation
	 * 
	 * @param mobilenumber
	 * @return it returns the result
	 */

	@Path("/userlogin")
	@POST
	@Produces("text/plain")
	public String userLogin(@QueryParam("mobilenumber") String mobilenumber) {

		// call service method --> DAO

		new UserService();
		boolean isExisted = UserService.registeredUser(mobilenumber);
		String result = isExisted ? "Valid User" : "user does not exist";

		return result;

	}

	/**
	 * This is resource method which performs user login operation
	 * 
	 * @param username
	 * @param password
	 * @return it returns the result
	 */

	@Path("/adminlogin")
	@POST
	@Produces("text/plain")
	public String AdminLogin(@QueryParam("username") String username, @QueryParam("password") String password) {

		boolean isExisted = new AdminServiceImpl().doLogin(username, password);

		String result = isExisted ? "Valid Admin" : "user does not exist";

		return result;

	}

}
