package com.abc;

public class Triangle implements Shape {

	private double length;
	private double breadth;
	
	public void setLength(double length) {
		this.length = length;
	}
	
	public void setBreadth(double breadth) {
		this.breadth = breadth;
	}

	@Override
	public double area() {
		return 0.5*length*breadth;
	}
	
}
