package com.abc;

public class ShapeArea {
	private Shape shape;
	
	public ShapeArea(Shape shape) {
		this.shape=shape;
	}
 public void calculateArea() {
	 
	 double result = shape.area();
	 System.out.println("Result is:" +result);
 }
 
}
