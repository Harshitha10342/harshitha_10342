package com.abc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abc.bean.Emp;
import com.abc.dao.EmpDAO;

@Service
public class EmpService {
	
	@Autowired
	private EmpDAO empDAO;
	
	
	public void setEmpDAO(EmpDAO empDAO) {
		this.empDAO = empDAO;
	}

	public Emp save() {
		Emp emp=empDAO.createEmp();
		return emp;
	}

}
