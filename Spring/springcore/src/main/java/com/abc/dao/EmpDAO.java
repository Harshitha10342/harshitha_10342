package com.abc.dao;

import org.springframework.stereotype.Repository;

import com.abc.bean.Emp;

@Repository
public class EmpDAO {
  
	public Emp createEmp() {
		
		Emp e1=new Emp();
		e1.setEmpId(111);
		e1.setFirstName("sharma");
		e1.setLastName("bora");
		
		return e1;
	}
	
}
