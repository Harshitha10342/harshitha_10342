package com.abc.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.abc.bean.Emp;
import com.abc.controller.EmpController;

public class EmpMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	 
	ApplicationContext context = new ClassPathXmlApplicationContext("emp.xml");	
	EmpController controller=(EmpController) context.getBean(EmpController.class); 
	
	Emp emp= controller.saveEmp();
	  
	  System.out.println("Emp saved:"+emp.getEmpId());
	  
	}

}
