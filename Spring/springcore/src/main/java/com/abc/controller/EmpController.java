package com.abc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.abc.bean.Emp;
import com.abc.service.EmpService;

@Controller
public class EmpController {
	
	 @Autowired
	 private EmpService service;
	 
	 public void setService(EmpService service) {
		 this.service=service;
	 }
	public Emp saveEmp() {
		
		return service.save();
	}

}
