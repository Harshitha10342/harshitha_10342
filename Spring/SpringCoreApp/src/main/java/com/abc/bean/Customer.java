package com.abc.bean;

public class Customer {
	
	private int customerId;
	private String name;
	public Customer(int customerId, String name) {
		super();
		this.customerId = customerId;
		this.name = name;
	}
	
	public void display() {
		System.out.println("Id:"+customerId+"name: "+name);
	}

}
