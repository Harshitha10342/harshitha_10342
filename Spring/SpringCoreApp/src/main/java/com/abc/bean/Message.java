package com.abc.bean;

public class Message {

	private String message;

	public void setMessage(String message) {
		this.message = message;
	}
	
	public void displayMessage() {
		System.out.println("Hello :"+message);
	}
}
