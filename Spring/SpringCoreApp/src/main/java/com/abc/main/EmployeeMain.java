package com.abc.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.abc.bean.Adress;
import com.abc.bean.Employee;

public class EmployeeMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ApplicationContext context = 
				new ClassPathXmlApplicationContext("classpath:com/abc/config/context.xml");
	   Employee emp=(Employee) context.getBean("emp");
	   
	   System.out.println("Employee details");
	   System.out.println(emp.getEmpId());
	   Adress empAddress =emp.getAddress();
	   System.out.println(empAddress.getCity());
	   
	}

}
