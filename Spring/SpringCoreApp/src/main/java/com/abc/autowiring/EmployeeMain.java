package com.abc.autowiring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class EmployeeMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ApplicationContext context = 
				new ClassPathXmlApplicationContext("classpath:com/abc/autowiring/application.xml");
	   Employee emp=(Employee) context.getBean("emp");
	   
	   System.out.println("Employee details");
	   System.out.println(emp.getEmpId());
	   Adress empAddress =emp.getAddr();
	   System.out.println(empAddress.getCity());
	   
	}

}
