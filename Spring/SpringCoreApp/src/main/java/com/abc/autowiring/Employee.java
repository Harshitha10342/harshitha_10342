package com.abc.autowiring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class Employee {
	
	private int empId;
	private String name;
	@Autowired
	@Qualifier("address2")
	private Adress addr;
	
	public int getEmpId() {
		return empId;
	}
	public void setEmpId(int empId) {
		this.empId = empId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Adress getAddr() {
		return addr;
	}
	public void setAddr(Adress addr) {
		this.addr = addr;
	}
	

}
