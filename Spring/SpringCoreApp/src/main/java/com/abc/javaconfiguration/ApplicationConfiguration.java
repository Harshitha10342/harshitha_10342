package com.abc.javaconfiguration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.abc.bean.Message;

@Configuration
public class ApplicationConfiguration {

	@Bean
	public Message getMessageBean() {
		Message message = new Message();
		message.setMessage("hello world");
		return message;
	}
	
	
}
