package com.abc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abc.dao.AccountDAO;
import com.abc.hibernate.entities.Account;

@Service
public class AccountServieImpl implements AccountService {

	@Autowired
	private AccountDAO accountDAO;

	@Transactional
	@Override
	public int insertAccount(Account account) {

		return accountDAO.createAccount(account);

	}
	
}
