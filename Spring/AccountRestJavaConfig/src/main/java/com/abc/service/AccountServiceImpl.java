package com.abc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abc.dao.AccountDAO;
import com.abc.hibernate.entities.Account;

/**
 * This is the service class which calls all the methods from dao package
 * 
 * @author IMVIZAG
 *
 */

//stereotype annotations
@Service
public class AccountServiceImpl implements AccountService {

	// Spring creates the object for AccountDAO
	@Autowired
	private AccountDAO accountDAO;

	@Transactional
	@Override
	public int insertAccount(Account account) {

		return accountDAO.createAccount(account);

	}

	@Transactional
	@Override
	public Account getAccountById(int accno) {

		return accountDAO.getAccountByAccNo(accno);
	}

	@Transactional
	@Override
	public void removeAccount(int accno) {

		accountDAO.deleteAccount(accno);

	}

	@Transactional
	@Override
	public void modifyAccount(Account account) {

		accountDAO.updateAccount(account);
	}

	@Transactional
	@Override
	public List<Account> displayAllAccounts() {

		return accountDAO.getAllAccounts();

	}

}
