package com.abc.service;

import java.util.List;

import com.abc.hibernate.entities.Account;

/**
 * This acts as the interface for the dao package
 * 
 * @author IMVIZAG
 *
 */
public interface AccountService {

	int insertAccount(Account account);

	Account getAccountById(int accno);

	void removeAccount(int accno);

	void modifyAccount(Account account);

	List<Account> displayAllAccounts();

}
