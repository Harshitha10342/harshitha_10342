package com.abc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.abc.hibernate.entities.Account;
import com.abc.service.AccountService;

//stereotype rest annotations
@RestController
public class AccountController {

	// Spring creates the object for AccountService
	@Autowired
	private AccountService accountService;

	@PostMapping("/create")
	public ResponseEntity<?> createNewAccount(@RequestBody Account account) {

		int id = accountService.insertAccount(account);
		return ResponseEntity.ok().body("New Account is created with Id: " + id);
	}

	@GetMapping("/displayId/{id}")
	public ResponseEntity<Account> getAccountId(@PathVariable("id") int accno) {

		Account account = accountService.getAccountById(accno);
		if (account == null) {
			return new ResponseEntity<Account>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<Account>(account, HttpStatus.FOUND);
		}
	}

	@DeleteMapping("/delete/{id}")
	public ResponseEntity<?> delete(@PathVariable("id") int accno) {

		Account account = accountService.getAccountById(accno);
		if (account == null) {
			return new ResponseEntity<Account>(HttpStatus.NOT_FOUND);
		} else {
			accountService.removeAccount(accno);
			return new ResponseEntity<Account>(HttpStatus.NO_CONTENT);
		}
	}

	@PutMapping("/modify/{id}")
	public ResponseEntity<?> modify(@PathVariable("id") int accno, @RequestBody Account account) {

		Account modifyAccount = accountService.getAccountById(accno);
		if (account == null) {
			return new ResponseEntity<Account>(HttpStatus.NOT_FOUND);
		} else {
			accountService.modifyAccount(modifyAccount);
			modifyAccount.setAccno(account.getAccno());
			modifyAccount.setName(account.getName());
			modifyAccount.setBalance(account.getBalance());
			return new ResponseEntity<Account>(modifyAccount, HttpStatus.OK);
		}
	}

	@GetMapping("/displayAll")
	public ResponseEntity<List<Account>> displayAll() {

		List<Account> account = accountService.displayAllAccounts();
		if (account.isEmpty()) {			
			return new ResponseEntity<List<Account>>(HttpStatus.NOT_FOUND);
		} else {		
			return new ResponseEntity<List<Account>>(account, HttpStatus.OK);
		}
	}
}
