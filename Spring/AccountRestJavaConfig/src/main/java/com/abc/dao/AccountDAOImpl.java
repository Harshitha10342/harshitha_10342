package com.abc.dao;

import java.util.Iterator;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.abc.hibernate.entities.Account;

/**
 * This class contains create, update, delete, display methods of the Account
 * 
 * @author IMVIZAG
 *
 */

//stereotype annotations
@Repository
public class AccountDAOImpl implements AccountDAO {

	// Spring creates the object for SessionFactory
	@Autowired
	private SessionFactory sessionFactory;

	/**
	 * this method is used to save details of the Account in the database
	 */
	@Override
	public int createAccount(Account account) {

		Session session = sessionFactory.getCurrentSession();
		session.save(account);
		return account.getAccno();
	}

	/**
	 * This method is used to find the accno from the database
	 */
	@Override
	public Account getAccountByAccNo(int accno) {

		Session session = sessionFactory.getCurrentSession();
		Account account = session.get(Account.class, accno);
		return account;
	}

	/**
	 * this method is used to delete the record from the database
	 */
	@Override
	public void deleteAccount(int accno) {

		Session session = sessionFactory.getCurrentSession();
		Account account = new Account();
		account.setAccno(accno);
		session.delete(account);
	}

	/**
	 * this method is used to update the record in the database
	 */
	@Override
	public void updateAccount(Account account) {

		Session session = sessionFactory.getCurrentSession();
		session.update(account);
	}

	/**
	 * This method is used to display all the products
	 */
	@Override
	public List<Account> getAllAccounts() {
		Session session = sessionFactory.getCurrentSession();
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Account> cq = cb.createQuery(Account.class);
		Root<Account> root = cq.from(Account.class);
		cq.select(root);
		Query<Account> query = session.createQuery(cq);
		return query.getResultList();
	}

//	@Override
//	public List<Account> getAllAccounts() {
//
//		Session session = sessionFactory.getCurrentSession();
//		List<Account> list = session.createQuery("FROM Account").list();
//		Iterator<Account> iterator = list.iterator();
//		while (iterator.hasNext()) {
//			Account account = (Account) iterator.next();
//			System.out.println("AccountNo: " + account.getAccno() + "\nAccountName: " + account.getName()
//					+ "\n AccountBalance: " + account.getBalance());
//
//		}
//		return null;
//	}

}
