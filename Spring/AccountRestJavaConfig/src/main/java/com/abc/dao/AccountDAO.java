package com.abc.dao;

import java.util.List;

import com.abc.hibernate.entities.Account;

/**
 * This acts as the interface for the dao package
 * 
 * @author IMVIZAG
 *
 */
public interface AccountDAO {

	int createAccount(Account account);

	Account getAccountByAccNo(int accno);
	
	void deleteAccount(int accno);
	
	void updateAccount(Account account);
	
	List<Account> getAllAccounts();
}