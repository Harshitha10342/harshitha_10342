package com.abc.controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class login {
	@GetMapping("loginform")
	public String getLoginForm() {
		return "login";
	}
	
	@PostMapping("/login")
	public String doLogin(@RequestParam("email") String email,@RequestParam("pwd") String pwd,ModelMap map) {
		
		if(pwd.equals("123")) {
			map.addAttribute("myemail", email);
			return "login_success";
		}
		else {
			return "login_failure";
		}
		
	}

}
