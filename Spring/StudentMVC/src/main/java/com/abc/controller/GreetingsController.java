package com.abc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class GreetingsController {
	
	@GetMapping("greet")
	public ModelAndView myGreetings() {
		
		String myData ="Spring mvc Greetings you";
		
		ModelAndView mav= new ModelAndView("Result","modelData",myData);
		return mav;
	}

}
