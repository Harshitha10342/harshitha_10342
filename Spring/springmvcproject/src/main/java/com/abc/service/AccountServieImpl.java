package com.abc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abc.dao.AccountDAO;
import com.abc.hibernate.entities.Account;

@Service
public class AccountServieImpl implements AccountService {

	@Autowired
	private AccountDAO accountDAO;

	@Transactional
	@Override
	public Account searchAccountByID(int accno) {

		return accountDAO.getAccountByID(accno);

	}

	@Transactional
	@Override
	public void insertAccount(Account account) {

		accountDAO.createAccount(account);

	}

	@Transactional
	@Override
	public void removeAccount(int accno) {

		accountDAO.deleteAccount(accno);

	}
	
	@Transactional
	@Override
	public boolean modifyAccount(Account account) {

		return accountDAO.updateAccount(account);

	}

}
