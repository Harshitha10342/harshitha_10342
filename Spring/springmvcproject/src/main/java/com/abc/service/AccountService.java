package com.abc.service;

import com.abc.hibernate.entities.Account;

public interface AccountService {

	Account searchAccountByID(int accno);

	void insertAccount(Account account);

	void removeAccount(int accno);

	boolean modifyAccount(Account account);
}
