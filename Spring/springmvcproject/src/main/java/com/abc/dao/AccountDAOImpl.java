package com.abc.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.abc.hibernate.entities.Account;

@Repository
public class AccountDAOImpl implements AccountDAO {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Account getAccountByID(int accno) {
		
		Session session = sessionFactory.getCurrentSession();
		Account account = session.get(Account.class, accno);
		return account;
	}

	@Override
	public void createAccount(Account account) {

		Session session = sessionFactory.getCurrentSession();
		session.save(account);
	}

	@Override
	public void deleteAccount(int accno) {

		Session session = sessionFactory.getCurrentSession();
		Account account = new Account();
		account.setAccno(accno);
		session.delete(account);

	}

	@Override
	public boolean updateAccount(Account account) {

		Session session = sessionFactory.getCurrentSession();
		session.merge(account);
		boolean flag = true;
		return flag;
	}

}
