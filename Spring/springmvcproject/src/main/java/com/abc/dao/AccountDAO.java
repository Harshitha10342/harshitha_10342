package com.abc.dao;

import com.abc.hibernate.entities.Account;

public interface AccountDAO {

	Account getAccountByID(int accno);

	void createAccount(Account account);

	void deleteAccount(int accno);

	boolean updateAccount(Account account);
}