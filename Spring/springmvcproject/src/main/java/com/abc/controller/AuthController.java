package com.abc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.abc.bean.UserDetails;

@Controller
public class AuthController {

	@GetMapping("loginform")
	public String getLoginForm() {
		return "login";
	}

	@GetMapping("registerform")
	public String getRegisterForm() {
		return "registration";
	}

	@PostMapping("/login")
	public String doLogin(@RequestParam("email") String email, @RequestParam("pwd") String pwd, ModelMap map) {

		if (pwd.equals("123")) {
			map.addAttribute("myemail", email);
			return "login_success";
		} else {
			return "login_failure";
		}

	}

	@PostMapping("/register")
	public String doRegister(@ModelAttribute UserDetails user, ModelMap map) {

		if (user.getPassword().length() < 3) {
			return "registration_failuer";
		} else {
//			map.addAttribute("userDetails", user);
			return "registration_success";
		}
	}
}