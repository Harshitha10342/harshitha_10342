package com.abc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.abc.hibernate.entities.Account;
import com.abc.service.AccountService;

@Controller
public class AccountController {

	@Autowired
	private AccountService accountService;

	@RequestMapping("/accounts/{id}")
	public String searchAccount(@PathVariable("id") int accno, ModelMap map) {
		Account account = accountService.searchAccountByID(accno);
		map.addAttribute("account", account);
		return "account_saved";
	}

	@RequestMapping("/accounts/{id}/{name}/{balance}")
	public String saveAccount(@PathVariable("id") int accno, @PathVariable("name") String name,
			@PathVariable("balance") int balance) {
		Account account = new Account();
		account.setAccno(accno);
		account.setName(name);
		account.setBalance(balance);
		accountService.insertAccount(account);
		// map.addAttribute("account", account);
		return "account_created";
	}

	@RequestMapping("/account/{accid}")
	public String delAccount(@PathVariable("accid") int accno) {
		accountService.removeAccount(accno);
		return "account_deleted";
	}

	
	@GetMapping("/modifyForm")
	public String modifyForm() {

		return "account_modified";
	}
	@PostMapping("/modify")
	public String modifyAcc(@ModelAttribute Account account,ModelMap map) {
		map.addAttribute("modify", accountService.modifyAccount(account));
		return "account_modified";
	}
}
