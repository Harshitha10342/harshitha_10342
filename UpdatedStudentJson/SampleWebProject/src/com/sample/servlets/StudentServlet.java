package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sample.bean.Address;
import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;
import com.sample.util.JsonConverter;

/**
 * Servlet implementation class StudentServlet
 */
@WebServlet("/StudentServlet")
public class StudentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * this servlet method is used for insertion and to get the request from the
	 * html page and sends back the response to the user in the form of html
	 */

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		int studentId = Integer.parseInt(request.getParameter("id"));
		String studentName = request.getParameter("name");
		int studentAge = Integer.parseInt(request.getParameter("age"));
		String city = request.getParameter("city");
		String state = request.getParameter("state");
		String pincode = request.getParameter("pincode");

		Address address = new Address();
		address.setCity(city);
		address.setState(state);
		address.setPincode(pincode);
		Student student = new Student();
		student.setId(studentId);
		student.setName(studentName);
		student.setAge(studentAge);
		student.setAddress(address);

		StudentService service = new StudentServiceImpl();

		boolean result = service.insertStudent(student);
		PrintWriter out = response.getWriter();

		JsonConverter jc = new JsonConverter();

		if (result) {

			String res = jc.convertToJsonObjectInsert(student);
			out.println("student inserted sucessfully");
			out.println(res);
		} else {
			out.println(" student insert failed");
		}

	}
}
