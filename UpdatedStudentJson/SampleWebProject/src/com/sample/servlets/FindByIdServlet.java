package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;
import com.sample.util.JsonConverter;

/**
 * Servlet implementation class FindByIdServlet
 */
@WebServlet("/FindByIdServlet")
public class FindByIdServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public FindByIdServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int studentid = Integer.parseInt(request.getParameter("id"));
		StudentService service = new StudentServiceImpl();
		Student st = service.findById(studentid);
//		RequestDispatcher rd = request.getRequestDispatcher("search_result.jsp");
		PrintWriter printWriter = response.getWriter();
		if (st != null) {
//			request.setAttribute("st", st);
//			rd.forward(request, response);
			JsonConverter converter = new JsonConverter();

			String res = converter.convertToJsonObject(st);

			printWriter.println(res);

		} else {
//			response.sendRedirect("error.html");
			printWriter.println("id not found");
		}

	}
}
