package com.hibernate.main;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.hibernate.entities.Book;
import com.hibernate.util.HibernateUtil;

public class BookMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Book book = new Book();
		book.setBook_name("java");
		book.setBook_price(1000);
		
		Book book1 = new Book();
		book1.setBook_name("UI");
		book1.setBook_price(900);

		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction txn = session.beginTransaction();
		session.save(book);
		session.save(book1);
		System.out.println("Book saved");
		
		Book myBook = session.get(Book.class, 2);
		System.out.println("Book Displayed: "+myBook.getBook_name()+  " "+myBook.getBook_price());
		
		txn.commit();
		session.close();
		
	}

}
