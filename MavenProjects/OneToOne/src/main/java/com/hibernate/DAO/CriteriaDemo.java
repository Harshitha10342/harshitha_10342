package com.hibernate.DAO;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.hibernate.entity.Address;
import com.hibernate.entity.Employees;
import com.hibernate.entity.Products;
import com.hibernate.util.HibernateUtil;

public class CriteriaDemo {
	public boolean insertProducts(Employees employee){
		SessionFactory sessionFactory=HibernateUtil.getSessionFactory();
		Session session=sessionFactory.openSession();
		Transaction t=session.beginTransaction();   
		session.save(employee);
		Address address=employee.getAddress();
		session.save(address);   
	    t.commit();   
	
		session.close();
		return true;
		
	}
	public List<Products> displayProducts(){
		SessionFactory sessionFactory=HibernateUtil.getSessionFactory();
		Session session=sessionFactory.openSession();
		Criteria crit=session.createCriteria(Products.class);
		List<Products> list=crit.list();
		
		session.close();
		return list;
		
	}
		public void fetch(){
			
		SessionFactory sessionFactory=HibernateUtil.getSessionFactory();
		Session session=sessionFactory.openSession();
		
		Employees employee=session.get(Employees.class,1);
		System.out.println(employee.getEmp_id());
		System.out.println(employee.getEmp_name());
		System.out.println(employee.getEmail_id());
		
		Address address=employee.getAddress();
		System.out.println(address.getCity());
		System.out.println(address.getState());
		session.save(employee);
		session.save(address);
		
		session.close();
		
		
	}
	

}
