package com.manytomany.entites;

import java.util.ArrayList;
import java.util.List;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import javax.persistence.JoinColumn;


@Entity
@Table
public class Stock {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	
	private int stockId;
	
	@Column(nullable=false)
	private String stockName;
	
	@Column(nullable=false)
	private int stockQty;
	
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "stock_category", joinColumns = { 
			@JoinColumn(name = "stockId", nullable = false, updatable = false) }, 
			inverseJoinColumns = { @JoinColumn(name = "categoriesId", 
					nullable = false, updatable = false) })

	List<Categories>categories= new ArrayList<Categories>();
	
	
	
	
	public int getStockId() {
		return stockId;
	}

	public void setStockId(int stockId) {
		this.stockId = stockId;
	}

	public String getStockName() {
		return stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	public int getStockQty() {
		return stockQty;
	}

	public void setStockQty(int stockQty) {
		this.stockQty = stockQty;
	}
		
	
	
	
	public List<Categories> getCategories() {
		return categories;
	}

	public void setCategories(List<Categories> categories) {
		this.categories = categories;
	}
	
	
	
}
