package com.manytomany.main;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.manytomany.entites.Categories;
import com.manytomany.entites.Stock;
import com.manytomany.util.HibernateUtil;


public class ManyToManyMain {

	public static void main(String[] args) {

		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		Stock stocks = new Stock();
		stocks.setStockName("raghu");
		stocks.setStockQty(512);
		List<Categories> categories = new ArrayList<Categories>();
		Categories categories1 = new Categories();
		categories1.setCategoriesName("sweet");

		Categories categories2 = new Categories();
		categories2.setCategoriesName("cool");

		
		categories.add(categories1);
		categories.add(categories2);

		stocks.setCategories(categories);
		
		session.save(stocks);
		
		System.out.println("sucess");

		transaction.commit();
		
		

	}

}
