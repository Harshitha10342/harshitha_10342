package com.abc.main;

import java.util.Scanner;

import com.abc.entity.Product;
import com.abc.service.ProductService;
/**
 * This is the main class from where the execution starts and calls all the methods from service package
 * @author IMVIZAG
 *
 */
public class Main {

	public static void main(String[] args) {
		// creating the scanner class for user input
		Scanner sc = new Scanner(System.in);

		Product product = new Product();
		ProductService productService = new ProductService();

		System.out.println("Enter your Choice");
		System.out.println("--------------------");
		System.out.println("1. Add Product \n2. Update Product \n3. Delete Product \n4. Display Product \n");
		System.out.println("--------------------");
		int choice = sc.nextInt();
		switch (choice) {
		case 1:
			product.setProductID("P107");
			product.setProductName("Redmi");
			product.setProductPrice(18000);
			product.setCategory("Mobiles");
			productService.createProduct(product);
			System.out.println("Product Added");
			break;

		case 2:
			System.out.println("Enter ID to Update");
			String updatedId = sc.next();
			product.setProductID(updatedId);
			System.out.println("Enter Name");
			String updatedName = sc.next();
			product.setProductName(updatedName);
			System.out.println("Enter Price");
			double updatedPrice = sc.nextDouble();
			product.setProductPrice(updatedPrice);
			product.setCategory("Mobiles");
			productService.updateProduct(product);
			System.out.println("Product Updated");
			break;

		case 3:
			productService.deleteProduct("P105");
			System.out.println("Product Deleted");
			break;

		case 4:
			productService.displayProduct(product);
			break;

		default:
			System.out.println("Invalid Choice");

		}
		//closing the Scanner
		sc.close();
	}
}
