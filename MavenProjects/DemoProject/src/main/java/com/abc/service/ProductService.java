package com.abc.service;

import java.util.List;

import com.abc.dao.ProductDAO;
import com.abc.entity.Product;

/**
 * This is the service package which calls all the methods from dao package
 * 
 * @author IMVIZAG
 *
 */
public class ProductService {

	ProductDAO productDAO;

	/**
	 * this constructor is used to create the object
	 */
	public ProductService() {
		productDAO = new ProductDAO();
	}

	/**
	 * this method calls the DAO package for creating the product
	 * @param product
	 * @return
	 */
	public boolean createProduct(Product product) {

		return productDAO.create(product);
	}

	/**
	 * this method calls the DAO package for deleting the product
	 * @param productID
	 * @return
	 */
	public boolean deleteProduct(String productID) {

		return productDAO.delete(productID);
	}

	/**
	 * this method calls the DAO package to update the product
	 * @param product
	 * @return
	 */
	public boolean updateProduct(Product product) {

		return productDAO.update(product);
	}

	/**
	 * this method calls the DAO package for displaying the product
	 * @param product
	 * @return
	 */
	public List<Product> displayProduct(Product product) {

		return productDAO.display();
	}
}
