package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class DisplayAllStudents
 */
@WebServlet("/DisplayAllStudents")
public class DisplayAllStudents extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	/**
	 * this servlet method is used for displaying and to get the request from the html
	 * page and sends back the response to the user in the form of html
	 */
	
   	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	   	
   		
   		StudentService service = new StudentServiceImpl();
   		List<Student> studentList = service.fetchAllStudents();
   		request.setAttribute("studentList", studentList);
   		RequestDispatcher rd = request.getRequestDispatcher("display_all.jsp");
   		rd.forward(request, response);
   	}

}
