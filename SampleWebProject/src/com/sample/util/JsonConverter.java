package com.sample.util;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import com.sample.bean.Student;

public class JsonConverter {

	private final Gson gson;

	public JsonConverter() {
		gson = new GsonBuilder().create();
	}

	// converting the Student List(display operations) to json object
	public String convertToJsonList(List<Student> studentList) {

		JsonArray jarray = gson.toJsonTree(studentList).getAsJsonArray();
		JsonObject jsonObject = new JsonObject();
		jsonObject.add("studentList", jarray);
		return jsonObject.toString();
	}

	// converting the Student Object(search by id) to json object
	public String convertToJsonObject(Student student) {

		String jarray = gson.toJson(student);

		return jarray;
	}

	// converting the Student Object(Insertion) to json object
	public String convertToJsonObjectInsert(Student student) {
		List<Student> list = new ArrayList<Student>();
		list.add(student);
		JsonArray jarray = gson.toJsonTree(list).getAsJsonArray();
		JsonObject jsonObject = new JsonObject();
		jsonObject.add("list", jarray);
		return jsonObject.toString();

	}

}
