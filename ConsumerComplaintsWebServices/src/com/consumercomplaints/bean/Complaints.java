package com.consumercomplaints.bean;


import java.util.*;
/**
 *  pojo class for storing user complaints
 * 
 * @author IMVIZAG
 *
 */
public class Complaints {
	private int reference_Id;
	private String complaint_Name;
	private int complaint_id;
	private String consumer_Id;
	private int service_Id;
	private Date Date_of_Complaint;
	
	public int getReference_Id() {
		return reference_Id;
	}
	public void setReference_Id(int reference_Id) {
		this.reference_Id = reference_Id;
	}
	public String getComplaint_Name() {
		return complaint_Name;
	}
	public void setComplaint_Name(String complaint_Name) {
		this.complaint_Name = complaint_Name;
	}
	public int getComplaint_id() {
		return complaint_id;
	}
	public void setComplaint_id(int complaint_id) {
		this.complaint_id = complaint_id;
	}
	public String getConsumer_Id() {
		return consumer_Id;
	}
	public void setConsumer_Id(String consumer_Id) {
		this.consumer_Id = consumer_Id;
	}
	public int getService_Id() {
		return service_Id;
	}
	public void setService_Id(int service_Id) {
		this.service_Id = service_Id;
	}
	public Date getDate_of_Complaint() {
		return  Date_of_Complaint;
	}
	public void setDate_of_Complaint(Date date) {
		Date_of_Complaint = date;
	}
	
}
