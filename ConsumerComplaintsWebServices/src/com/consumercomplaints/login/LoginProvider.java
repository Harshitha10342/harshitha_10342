package com.consumercomplaints.login;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

import com.consumercomplaints.services.UserServicesIMPL;
import com.sun.jersey.api.view.Viewable;
import com.sun.research.ws.wadl.Response;

@Path("/login")
public class LoginProvider {
	@GET
	public Viewable login(@QueryParam("username") String username, @QueryParam("password") String password) {
		
		Viewable view = null;
		boolean isExisted = new UserServicesIMPL().login(username,password);
		//String result =  isExisted ? "valid user" : "invalid user" ;
		
		if(isExisted) {
		 view = new Viewable("/Home.html");
		}
		else {
			 view  = new Viewable("/error.html");
		}
		
		return view;
	}
	

}
