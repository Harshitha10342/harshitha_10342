<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Questions Created</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>
<body>
	<form action="create">
		<div class="form-group">
			<h4>Enter UserId:</h4>
			<input type="text" class="form-control" placeholder="Enter UserId">
			<label for="Question create">
				<h4>Enter your Question:</h4>
			</label>
			<textarea class="form-control" rows="5"></textarea>
			<h4>Category</h4>
			<select>
				<option value="volvo">Social</option>
				<option value="saab">Entertainment</option>
				<option value="opel">Technology</option>
				<option value="audi">Health</option>
			</select>
		</div>
		<button type="submit" class="btn btn-primary">create</button>


	</form>
</body>
</html>