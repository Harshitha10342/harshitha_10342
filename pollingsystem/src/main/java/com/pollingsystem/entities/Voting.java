package com.pollingsystem.entities;



import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * This class is used to store voting entity which creates voting schema in
 * database
 * 
 * @author IMVIZAG
 *
 */
@Entity
@Table(name = "voting_tbl")
public class Voting {

	// declaring columns in table
	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int voting_id;
	@ManyToOne
	@JoinColumn(name = "user_id")
	private UserDetails userdetails;

	@ManyToOne
	@JoinColumn(name = "question_id")
	private Question question;

	@ManyToOne
	@JoinColumn(name = "answer_id")
	private AnswerEntity answer;

	private Date date;

	/**
	 * @return the voting_id
	 */
	public int getVoting_id() {
		return voting_id;
	}

	/**
	 * @param voting_id the voting_id to set
	 */
	public void setVoting_id(int voting_id) {
		this.voting_id = voting_id;
	}

	/**
	 * @return the userdetails
	 */
	public UserDetails getUserdetails() {
		return userdetails;
	}

	/**
	 * @param userdetails the userdetails to set
	 */
	public void setUserdetails(UserDetails userdetails) {
		this.userdetails = userdetails;
	}

	/**
	 * @return the question
	 */
	public Question getQuestion() {
		return question;
	}

	/**
	 * @param question the question to set
	 */
	public void setQuestion(Question question) {
		this.question = question;
	}

	/**
	 * @return the answer
	 */
	public AnswerEntity getAnswer() {
		return answer;
	}

	/**
	 * @param answer the answer to set
	 */
	public void setAnswer(AnswerEntity answer) {
		this.answer = answer;
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

}
