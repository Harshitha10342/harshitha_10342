package com.pollingsystem.util;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateServletContextListener implements ServletContextListener {
private static SessionFactory sessionFactory;
	@Override
	public void contextDestroyed(ServletContextEvent sce) {

}


	
@Override
	public void contextInitialized(ServletContextEvent sce) {
		try {

			sessionFactory = new Configuration().configure().buildSessionFactory();
		} catch (Throwable ex) {
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}

}
