package com.pollingsystem.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.pollingsystem.entities.UserDetails;
import com.pollingsystem.util.HibernateServletContextListener;

public class UserDaoImpl implements UserDao {


	@Override
	public UserDetails getUserByCredentials(String userId, String password) {
		
	    SessionFactory factory = HibernateServletContextListener.getSessionFactory();
	    Session session = factory.openSession();
		
		Transaction tx = session.beginTransaction();
		Query query = session.createQuery("from UserDetails where user_id=:user_id and password=:password");
		query.setString("user_id", userId);
		query.setString("password", password);
		UserDetails user = (UserDetails) query.uniqueResult();
		if (user != null) {
			System.out.println("User Retrieved from DB::" + user);
		}
		tx.commit();
		session.close();
		return user;
	}

	@Override
	public UserDetails updateDetails(UserDetails users) {

	    SessionFactory factory = HibernateServletContextListener.getSessionFactory();
	    Session session = factory.openSession();
		
		Transaction tx = session.beginTransaction();
		session.save(users);
		return users;
	}

}
