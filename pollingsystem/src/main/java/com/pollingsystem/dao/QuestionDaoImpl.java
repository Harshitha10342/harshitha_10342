package com.pollingsystem.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.sql.Insert;

import com.pollingsystem.entities.Question;
import com.pollingsystem.util.HibernateServletContextListener;

public class QuestionDaoImpl implements QuestionDao {
	/**
	 * This method is used to create a question by user.
	 */
	public boolean createQuestion(Question question) {
		SessionFactory factory = HibernateServletContextListener.getSessionFactory();
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
	
	   session.save(question);
	   tx.commit();

		session.close();
		return true;
	
      
		
	}

	
	

}
