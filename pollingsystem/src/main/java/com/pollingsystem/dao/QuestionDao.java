package com.pollingsystem.dao;

import com.pollingsystem.entities.Question;

/**
 * This class is used as a DAO which extends crud repository
 * 
 * @author IMVIZAG
 *
 */
public interface QuestionDao  {
	boolean  createQuestion(Question question);
	

	
}