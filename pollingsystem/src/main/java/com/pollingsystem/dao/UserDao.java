package com.pollingsystem.dao;

import com.pollingsystem.entities.UserDetails;

/**
 * This class is used as a DAO which extends crud repository
 * 
 * @author IMVIZAG
 *
 */
public interface UserDao  {
	 UserDetails getUserByCredentials(String string, String password);

	UserDetails updateDetails(UserDetails users);
}