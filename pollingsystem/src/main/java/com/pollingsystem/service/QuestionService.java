package com.pollingsystem.service;

import com.pollingsystem.dao.QuestionDao;
import com.pollingsystem.dao.QuestionDaoImpl;
import com.pollingsystem.entities.Question;

/**
 * This Class is used to provide services for Questions
 * 
 * @author PollingStar
 *
 */
public class QuestionService {
	private QuestionDao questionDao  = new QuestionDaoImpl();

	public void createQuestion(Question question) {
		
		questionDao.createQuestion(question);
		
		
	}
	
}
