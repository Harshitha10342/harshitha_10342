package com.pollingsystem.action;

import javax.servlet.ServletContext;

import org.apache.struts2.util.ServletContextAware;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ModelDriven;
import com.pollingsystem.dao.QuestionDao;
import com.pollingsystem.dao.QuestionDaoImpl;
import com.pollingsystem.entities.Question;
/**
 * This method is used to create a query object into a Question table 
 * and returns the question object.
 * @author IMVIZAG
 *
 */
public class CreateAction implements Action, ModelDriven<Question>, ServletContextAware {
	private int userId;
	 QuestionDao quedao=new QuestionDaoImpl();
	@Override
	public String execute() throws Exception {
		 
			boolean result = quedao.createQuestion(question);
			if(result) {
				return SUCCESS;
			}
			else 
				return ERROR;
			}
	@Override
	public Question getModel() {
		
		return question;
	}
	
	private Question question = new Question();
	
	private ServletContext ctx;

	@Override
	public void setServletContext(ServletContext sc) {
		this.ctx = sc;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public Question getQuestion() {
		return question;
	}
	public void setQuestion(Question question) {
		this.question = question;
	}
		
		
	}


