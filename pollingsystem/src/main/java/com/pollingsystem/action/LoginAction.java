package com.pollingsystem.action;

import javax.servlet.ServletContext;

import org.apache.struts2.util.ServletContextAware;
import org.hibernate.SessionFactory;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ModelDriven;
import com.pollingsystem.dao.UserDao;
import com.pollingsystem.dao.UserDaoImpl;
import com.pollingsystem.entities.UserDetails;

public class LoginAction implements Action, ModelDriven<UserDetails>, ServletContextAware {
	@Override
	public String execute() throws Exception {
		

		UserDao userDAO = new UserDaoImpl();
		UserDetails userDB = userDAO.getUserByCredentials(user.getUser_id(), user.getPassword());
		if(userDB == null) 
			return ERROR;
		else {
			user.setEmail(userDB.getEmail());
			user.setFirst_name(userDB.getFirst_name());
			return SUCCESS;
		}
	}

	@Override
	public UserDetails getModel() {
		
		return user;
	}
	
	private UserDetails user = new UserDetails();
	
	private ServletContext ctx;

	@Override
	public void setServletContext(ServletContext sc) {
		this.ctx = sc;
	}
}
