package com.pollingsystem.action;

import javax.servlet.ServletContext;

import org.apache.struts2.util.ServletContextAware;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ModelDriven;
import com.pollingsystem.dao.UserDao;
import com.pollingsystem.dao.UserDaoImpl;
import com.pollingsystem.entities.UserDetails;

public class UpdateAction implements Action, ModelDriven<UserDetails>, ServletContextAware {
	@Override
	public String execute() throws Exception {

		UserDao userDAO = new UserDaoImpl();
		UserDetails userDB = userDAO.updateDetails(users);
		return SUCCESS;

	}

	@Override
	public UserDetails getModel() {

		return users;
	}

	private UserDetails users = new UserDetails();

	private ServletContext ctx;

	@Override
	public void setServletContext(ServletContext sc) {
		this.ctx = sc;
	}
}
